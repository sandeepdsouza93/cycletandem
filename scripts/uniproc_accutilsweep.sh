#!/bin/bash
NUM_TASKS=5000
HARMONIC_FLAG=0
FILENAME="gpu_util_sweep.txt"
EPSILON=0.01
CPU_UTIL=0.4
for util in {10..70..10}
do
../src/uniprocessor_exp $NUM_TASKS $HARMONIC_FLAG $FILENAME $EPSILON $CPU_UTIL 0.$util
done
exit 0