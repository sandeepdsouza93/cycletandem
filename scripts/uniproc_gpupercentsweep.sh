#!/bin/bash
NUM_TASKS=5000
HARMONIC_FLAG=0
FILENAME="gpu_percent_sweep.txt"
EPSILON=0.01
CPU_UTIL=0.4
GPU_UTIL=0.3
for percent in {0..100..10}
do
GPU_PERCENT_TASKS=$(echo "scale = 1; $percent/100" | bc)
echo $GPU_PERCENT_TASKS
../src/uniprocessor_exp $NUM_TASKS $HARMONIC_FLAG $FILENAME $EPSILON $CPU_UTIL $GPU_UTIL $GPU_PERCENT_TASKS
done
exit 0