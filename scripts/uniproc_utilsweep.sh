#!/bin/bash
NUM_TASKS=5000
HARMONIC_FLAG=0
FILENAME="cpu_util_sweep.txt"
EPSILON=0.01
GPU_UTIL=0.3
for util in {10..70..10}
do
../src/uniprocessor_exp $NUM_TASKS $HARMONIC_FLAG $FILENAME $EPSILON 0.$util $GPU_UTIL
done
exit 0