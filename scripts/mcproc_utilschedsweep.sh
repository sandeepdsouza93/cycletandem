#!/bin/bash
NUM_TASKS=5000
HARMONIC_FLAG=0
FILENAME="mccpu_util_sched_sweep.txt"
EPSILON=0.01
GPU_UTIL=0.3
NUM_CORES=4
for util in {40..300..10}
do
CPU_UTIL=$(echo "scale = 1; $util/100" | bc)
echo $CPU_UTIL
../src/mcprocessor_sched_exp $NUM_TASKS $NUM_CORES $HARMONIC_FLAG $FILENAME $EPSILON $CPU_UTIL $GPU_UTIL 
done
exit 0