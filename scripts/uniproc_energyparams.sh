#!/bin/bash
NUM_TASKS=5000
HARMONIC_FLAG=0
FILENAME="energy_params_sweep.txt"
EPSILON=0.01
CPU_UTIL=0.4
GPU_UTIL=0.3
GPU_PERCENT_TASKS=0.5
Kc=(1 2 1 100 1 2)
Kg=(2 1 100 1 2 1) 
ENERGY_FACTOR=(2 2 2 2 3 3)
for index in {0..5..1}
do
	echo "Energy Params " ${Kc[$index]} ${Kg[$index]} ${ENERGY_FACTOR[$index]}
    ../src/uniprocessor_exp $NUM_TASKS $HARMONIC_FLAG $FILENAME $EPSILON $CPU_UTIL $GPU_UTIL $GPU_PERCENT_TASKS ${Kc[$index]} ${Kg[$index]} ${ENERGY_FACTOR[$index]}
done
exit 0