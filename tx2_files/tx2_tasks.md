# Calculating the constants
The GPU and GPU are on the same SoC, same technology so the energy consumption can be assumed to be similar

Note: All task values correspond to the highest CPU (Cortex A57 cores) and GPU frequency
Max CPU Frequency = 2,035,200 KHz
Max GPU Frequency = 1,134,750,000 Hz 
All Tasks are Matrix Multiplication Tasks, Follow RMS priority ordering and WFD for partitioning
# Taskset 1 #
C=10, T=100, Ge=5.46, Gm=0.19,  
C=20, T=150, Ge=8.68, Gm=0.19
C=30, T=300, Ge=29.74, Gm=0.22

# GPU Execution Time Mat-Mul Measurements
100 -> 3.46, 3.11  			(4, 0.3)
150 -> 5.58, 5.35			(6, 0.3)
200 -> 8.76, 8.45		    (9, 0.3)
250 -> 12.63, 12.45			(13, 0.3)
300 -> 17.53, 17.34			(18, 0.3)
400 -> 29.99, 29.81			(30, 0.3)
500 -> 46.15, 45.98		    (47, 0.3)
700 -> 88.90, 88.73			(89, 0.3)
900 -> 146.05, 145.87		(147, 0.3)
1000 -> 179.89, 179.72		(180, 0.3)
1200 -> 258.47, 258.29	    (259, 0.3)
1300 -> 303.18, 302.99		(304, 0.3)
1500 -> 403.81, 403.12		(404, 0.3)
2000 -> 714.95, 714.77		(715, 0.3)

# Workzone
GPU Exec Time -> 3.93, 1.75
CPU Exec -> 22.28

# Example Taskset1 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
(20,100)=0.2   		(100->4)=0.04
(10,100)=0.1		(150->6)=0.03
(30,150)=0.2		(100->4)=0.0266
(50,200)=0.25		(300->18)=0.09
(50,300)=0.166		(200->9)=0.03
(100,300)=0.33		(400->30)=0.1
(100,600)=0.166		(250->13)=0.021
(400,1200)=0.33		(400->30)=0.025
(CPU Util, GPU Util) = (1.75, 0.39) 

CycleSolo-CPU: CPU Frequency = 0.832022 -> 1728000
CycleSolo-Accel: Accel Frequency = 0.859375 -> 1032750000
CycleSolo-ID: ID Frequency = 0.896565 -> 1881600, 1032750000 
CycleTandem-Brute: 
CPU Frequency = 0.835938 -> 1728000
Accel Frequency = 0.989375 -> 1134750000

# Energy at Highest Frequency
Total Energy = 190073110.000000 uJ = 1826.39 mW
CPU Energy   = 139283670.000000 uJ = 1338.36 mW
GPU Energy   = 50789440.000000 uJ  =  488.03 mW
Time         = 104070 ms

# CycleSolo-Accel Energy
Total Energy = 187815330.000000 uJ = 1805.91
CPU Energy   = 141775910.000000 uJ = 1363.23
GPU Energy   = 46039420.000000 uJ = 442.65
Time         = 104000 ms

# CycleSolo-CPU Energy
Total Energy = 149677240.000000 uJ = 1435.20
CPU Energy   = 103776510.000000 uJ = 995.07
GPU Energy   = 45900730.000000 uJ = 440.12
Time         = 104290 ms

# CycleSolo-ID Energy
Total Energy = 166714540.000000 uJ = 1601.79
CPU Energy   = 120742940.000000 uJ = 1160.09
GPU Energy   = 45971600.000000 uJ = 441.69
Time         = 104080 ms

# CycleTandem-Brute
Total Energy = 142994740.000000 uJ = 1376.93
CPU Energy   = 98496580.000000 uJ = 948.450
GPU Energy   = 44498160.000000 uJ = 428.48
Time         = 103850 ms

# Example Taskset2 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
(10,150)=0.06   		(250->13)=0.0866
(50,300)=0.16		    (100->4)=0.0133
(60,600)=0.1		    (300->18)=0.03
(125,1200)=0.1  		(200->9)=0.0075
(CPU Util, GPU Util) = (0.42, 0.14) 

CPU Util   = 0.44125
Accel Util = 0.1375
CycleSolo-CPU: CPU Frequency = 0.195557 -> 499200
CycleSolo-Accel: Accel Frequency = 0.250781 -> 318750000
CycleSolo-ID: ID Frequency = 0.332549 -> 806400, 420750000
CycleTandem-Brute: 
CPU Frequency = 0.245283 -> 499200
Accel Frequency = 0.460674 -> 522750000

# Energy at Highest Frequency
Total Energy = 93622850.000000 uJ = 906.23
CPU Energy   = 62705490.000000 uJ = 607.14
GPU Energy   = 30917360.000000 uJ = 299.09
Time         = 103310 ms

# CycleSolo-Accel Energy
Total Energy = 86399030.000000 uJ = 835.82
CPU Energy   = 63097480.000000 uJ = 610.40
GPU Energy   = 23301550.000000 uJ = 225.41
Time         = 103370 ms


# CycleSolo-CPU Energy
Total Energy = 64510050.000000 uJ = 607.38
CPU Energy   = 32506990.000000 uJ = 306.06
GPU Energy   = 32003060.000000 uJ = 301.32
Time         = 106210 ms

#CycleSolo-ID
Total Energy = 56251360.000000 uJ = 537.31
CPU Energy   = 32668540.000000 uJ = 312.01
GPU Energy   = 23582820.000000 uJ = 225.30
Time         = 104690 ms


#CycleTandem
Total Energy = 56330670.000000 uJ = 530.12
CPU Energy   = 32472220.000000 uJ = 305.59
GPU Energy   = 23858450.000000 uJ = 224.53
Time         = 106260 ms


# Example Taskset3 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
//(40,150)=0.27   	(100->4)=0.0266
//(60,300)=0.2		(100->4)=0.0133
(200,450)=0.44  	(100->4)=0.008
(300,600)=0.5		(100->4)=0.0066
(400,900)=0.44  	(100->4)=0.0044
(1000,1800)=0.55	(100->4)=0.0022
(CPU Util, GPU Util) = (2.2, 0.052) 

CPU Util   = 1.94611
Accel Util = 0.0222222
CycleSolo-CPU: CPU Frequency = 0.56126 -> 1267200
CycleSolo-Accel: Accel Frequency = 0.047221 -> 114750000
CycleSolo-ID: ID Frequency = 0.565703 -> 1267200, 726750000
CycleTandem-Brute: 
CPU Frequency = 0.622642 -> 1267200
Accel Frequency = 0.191011 -> 216750000

# Energy at Highest Frequency
Total Energy = 220037650.000000 uJ = 1405.27
CPU Energy   = 189760460.000000 uJ = 1211.90
GPU Energy   = 30277190.000000 uJ =   193.37
Time         = 156580 ms

#CycleSolo-Accel Energy
Total Energy = 213596810.000000 uJ = 1363.00
CPU Energy   = 189583990.000000 uJ = 1209.77
GPU Energy   = 24012820.000000 uJ = 153.23
Time         = 156710 ms

#CycleSolo-CPU Energy
Total Energy = 125704240.000000 uJ = 797.32
CPU Energy   = 100285550.000000 uJ = 636.12
GPU Energy   = 25418690.000000 uJ =  161.20
Time         = 157650 ms

#CycleSolo-ID Energy
Total Energy = 123662290.000000 uJ = 784.31
CPU Energy   = 99279990.000000 uJ = 629.66
GPU Energy   = 24382300.000000 uJ = 154.64
Time         = 157600 ms

#CycleTandem Energy
Total Energy = 123414970.000000 uJ = 782.74
CPU Energy   = 99259120.000000 uJ = 629.53
GPU Energy   = 24155850.000000 uJ = 153.20
Time         = 157670 ms

# Example Taskset4 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
(2,250)=0.013   	(300->18)=0.072
(4,250)=0.026		(400->30)=0.12
(10,500)=0.02  	    (500->47)=0.094
(20,500)=0.04		(500->47)=0.094
(10,750)=0.013  	(700->89)=0.118
(10,1500)=0.002	    (400->30)=0.02
(CPU Util, GPU Util) = (0.1, 0.518) 

CPU Util   = 0.1082
Accel Util = 0.518667
CycleSolo-CPU: CPU Frequency = 0.0820651 -> 345600
CycleSolo-Accel: Accel Frequency = 0.773438 -> 930750000
CycleSolo-ID: ID Frequency = 0.78125 -> 1728000, 930750000
CycleTandem-Brute: 
CPU Frequency = 0.320755 -> 652800
Accel Frequency = 0.820225 -> 930750000

# Highest Frequency Energy
Total Energy = 64440430.000000 uJ = 632.57 
CPU Energy   = 47819500.000000 uJ = 469.41
GPU Energy   = 16620930.000000 uJ = 163.15
Time         = 101870 ms

#CycleSolo-Accel Energy
Total Energy = 63673010.000000 uJ = 625.28
CPU Energy   = 47654200.000000 uJ = 467.97
GPU Energy   = 16018810.000000 uJ = 157.30
Time         = 101830 ms

#CycleSolo-CPU Energy
Total Energy = 44504180.000000 uJ = 419.61
CPU Energy   = 25343980.000000 uJ = 238.95
GPU Energy   = 19160200.000000 uJ = 180.65
Time         = 106060 ms

#CycleSolo-ID Energy
Total Energy = 55052800.000000 uJ = 539.78
CPU Energy   = 38888540.000000 uJ = 381.29
GPU Energy   = 16164260.000000 uJ = 158.48
Time         = 101990 ms

#CycleTandem Energy
Total Energy = 41946450.000000 uJ = 404.65
CPU Energy   = 25382690.000000 uJ = 244.86
GPU Energy   = 16563760.000000 uJ = 159.78
Time         = 103660 ms