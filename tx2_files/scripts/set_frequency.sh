#!/bin/bash
CPU_MIN_FREQ="cpufreq/scaling_min_freq"
CPU_MAX_FREQ="cpufreq/scaling_max_freq"
CPU_CUR_FREQ="cpufreq/scaling_cur_freq"
CPU_AVAIL_FREQ="cpufreq/scaling_available_frequencies"
CPU_FOLDER="/sys/devices/system/cpu/cpu0/"
CPU_MAXFREQ=2035200

GPU_MIN_FREQ="/sys/devices/17000000.gp10b/devfreq/17000000.gp10b/min_freq"
GPU_MAX_FREQ="/sys/devices/17000000.gp10b/devfreq/17000000.gp10b/max_freq"
GPU_CUR_FREQ="/sys/devices/17000000.gp10b/devfreq/17000000.gp10b/cur_freq"
GPU_AVAIL_FREQ="/sys/devices/17000000.gp10b/devfreq/17000000.gp10b/available_frequencies"
GPU_MAXFREQ=1134750000

echo "Available Cortex A57 CPU Frequencies (KHz):"
cat "${CPU_FOLDER}/${CPU_AVAIL_FREQ}"
read -p "Enter Valid CPU Frequency: "  cpu_freq
echo "Setting Cortex A57 CPU Cluster Frequency to $cpu_freq"
echo $CPU_MAXFREQ > ${CPU_FOLDER}/${CPU_MAX_FREQ}
echo $cpu_freq > ${CPU_FOLDER}/${CPU_MIN_FREQ}
echo $cpu_freq > ${CPU_FOLDER}/${CPU_MAX_FREQ}
echo "Cortex A57 Cluster frequency set to:"
cat $CPU_FOLDER/$CPU_CUR_FREQ

echo "Available TX2 GPU Frequencies (Hz):"
cat $GPU_AVAIL_FREQ
read -p "Enter Valid GPU Frequency: "  gpu_freq
echo "Setting TX2 GPU Frequency to $gpu_freq"
echo $GPU_MAXFREQ > ${GPU_MAX_FREQ}
echo $gpu_freq > ${GPU_MIN_FREQ}
echo $gpu_freq > ${GPU_MAX_FREQ}
echo "TX2 GPU frequency set to:"
cat $GPU_CUR_FREQ
