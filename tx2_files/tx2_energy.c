#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <time.h>
#include <signal.h>

static volatile int running = 1;

void intHandler(int dummy) {
    running = 0;
}


// Convert a millisecond value to timespec
static void add_ms2ts(struct timespec *ts, unsigned long ms)
{
    struct timespec temp;
    temp.tv_sec = ts->tv_sec + ms/1000;
    temp.tv_nsec = ts->tv_nsec + (ms % 1000) * 1000000;

    ts->tv_sec = temp.tv_sec + (temp.tv_nsec/1000000000);
    ts->tv_nsec = temp.tv_nsec % 1000000000;
}

int main(int argc, char **argv) 
{
    int step_size_ms = 10;
    if (argc > 1)
        step_size_ms = atoi(argv[1]);

    int cnt = 0;
    int cpu_cnt = 0;
    int gpu_cnt = 0;
    double energy_gpu = 0;
    double energy_cpu = 0;
    double energy = 0;

    struct timespec tv;

    FILE *output_file;
    output_file = fopen("energy.txt","w");

    // Create the file descriptors to read the input power
    int fd_gpu = open("/sys/devices/3160000.i2c/i2c-0/0-0040/iio_device/in_power0_input", O_RDONLY | O_NONBLOCK);
    int fd_cpu = open("/sys/devices/3160000.i2c/i2c-0/0-0041/iio_device/in_power1_input", O_RDONLY | O_NONBLOCK);
    
    // Check if the file descriptors are valid
    if (fd_gpu < 0 || fd_cpu < 0) {
        perror("open()");
        exit(1);
    }

    // Install Signal Handler
    signal(SIGINT, intHandler);

    while (running) {
        if (cnt == 0) {
            clock_gettime(CLOCK_MONOTONIC, &tv);
        }

        char buf[31];
        lseek(fd_gpu, 0, 0);
        int n = read(fd_gpu, buf, 32);
        if (n > 0) {
            buf[n] = 0;
            char *o = NULL;
            energy_gpu += strtod(buf, &o)*(cnt-gpu_cnt+1)*step_size_ms; // Assuming power is in mw -> this gives energy in micro joule
            gpu_cnt = cnt + 1;
        }

        lseek(fd_cpu, 0, 0);
        n = read(fd_cpu, buf, 32);
        if (n > 0) {
            buf[n] = 0;
            char *o = NULL;
            energy_cpu += strtod(buf, &o)*(cnt-cpu_cnt+1)*step_size_ms; // Assuming power is in mw -> this gives energy in micro joule
            cpu_cnt = cnt + 1;
        }

        cnt = cnt + 1;
        energy = energy_cpu + energy_gpu;
        // Periodically take a reading every step size 
        add_ms2ts(&tv, (unsigned long)step_size_ms);
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &tv, NULL);
        
        if (running == 0)
        {
            printf("Total Energy = %f uJ\n", energy);
            printf("CPU Energy   = %f uJ\n", energy_cpu);
            printf("GPU Energy   = %f uJ\n", energy_gpu);
            printf("Time         = %d ms\n", cnt*10);
            fprintf(output_file, "Total Energy = %f uJ\n", energy);
            fprintf(output_file, "CPU Energy   = %f uJ\n", energy_cpu);
            fprintf(output_file, "GPU Energy   = %f uJ\n", energy_gpu);
	    fprintf(output_file, "Time         = %d ms\n", cnt*10);
            fclose(output_file);
        }

    }
    return 0;
}
