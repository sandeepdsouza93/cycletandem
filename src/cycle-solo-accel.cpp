/*
 * @file cycle-solo-accel.cpp
 * @brief CycleSolo-Accel implementation
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* Standard Library Imports */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <chrono>

/* Internal Headers */
#include "task.hpp"
#include "taskset.hpp"
#include "taskset-gen.hpp"
#include "request-driven-test.hpp"
#include "binary-search.hpp"
#include "config.hpp"

/* Find idle duration in nearest period/deadline boundary */
double find_min_idle_csa(int index, const std::vector<Task> &task_vector, const std::vector<double> &resp_time_hp, double resp_time, double deadline)
{
	double min_idle = deadline - resp_time;
	double idle;
	double susp_term;

	for (unsigned int i = 0; i < index; i++)
	{
		if (task_vector[i].getGe() != 0)
			susp_term = resp_time_hp[i] - (task_vector[i].getC()+task_vector[i].getGm());
		else
			susp_term = 0;		
		
		idle = task_vector[i].getT()*ceil((resp_time + susp_term)/task_vector[i].getT()) - resp_time - susp_term;
		if (idle < min_idle && idle > 0.0001)
		{
			min_idle = idle;
		}
	}
	return min_idle;
}

/**************** The CycleSolo-Accel Calculate Blocking sub-routine ********************/ 
double calculate_blocking_csa(unsigned int index, const std::vector<Task> &task_vector, double f_est, const std::vector<double> &resp_time_hp, double* cpu_intervention)
{
	double blocking, Gl_max, blocking_dash;
	/* Find index of maximum low priority GPU segment */
	int Gl_max_index = find_max_lp_gpu_index(index, task_vector);	

	// Return 0 blocking if task has no GPU execution
	if(task_vector[index].getGe() == 0)
		return 0;

	/* Note: Under this analysis even low-priority GPU access 
	         faces blocking from high-priority tasks */
	/* Refactor the maximum low priority blocking to take into account the CPU frequency */
	if (Gl_max_index < task_vector.size())
	{
		blocking = (task_vector[Gl_max_index].getGe()/f_est) + task_vector[Gl_max_index].getGm();
		Gl_max = blocking;
		// Ensure the initial value of blocking_dash is different from blocking
		if (Gl_max == 0)
		{
			blocking_dash = 1;
		}
		else
		{
			blocking_dash = 0;
		}

		// Initialize CPU Intervention
		*cpu_intervention = task_vector[Gl_max_index].getGm();
	}
	else
	{
		// No other lower priority tasks exist -> Vector Index has overrun
		blocking = 0;
		Gl_max = 0;
		// Ensure the initial value of blocking_dash is different from blocking
		blocking_dash = 1;

		// Initialize CPU Intervention
		*cpu_intervention = 0;
	}
	
	// Estimate Blocking
	while (blocking != blocking_dash)
	{
		blocking_dash = blocking;
		blocking = Gl_max;
		for (unsigned int i = 0; i < index; i++)
		{
			if (task_vector[i].getGe() != 0)
			{
				blocking = blocking 
			    + ceil((blocking_dash + resp_time_hp[i] - (task_vector[i].getC()+task_vector[i].getGm()))/task_vector[i].getT())*((task_vector[i].getGe()/f_est)+task_vector[i].getGm());
				*cpu_intervention = *cpu_intervention + 
				  ceil((blocking_dash + resp_time_hp[i] - (task_vector[i].getC()+task_vector[i].getGm()))/task_vector[i].getT())*(task_vector[i].getGm());
			}
		}
	}
	return blocking;
}

/**************** Calculate High-Priority Interference sub-routine with applying the scaling factor ********************/ 
double calculate_interference_scaled_csa(unsigned int index, const std::vector<Task> &task_vector, const std::vector<double> &resp_time_hp, double resp_time)
{
	double interference = 0;

	for (unsigned int i = 0; i < index; i++)
	{
		if (task_vector[i].getGe() != 0)
		{
			interference = interference + ceil((resp_time + resp_time_hp[i] - (task_vector[i].getC()+task_vector[i].getGm()))/task_vector[i].getT())*(task_vector[i].getC()+task_vector[i].getGm());	
		}
		else
			interference = interference + ceil((resp_time)/task_vector[i].getT())*(task_vector[i].getC());
	}
	return interference;
}

/**************** The CycleSolo-Accel Calculate High-Priority Interference sub-routine ********************/ 
double calculate_interference_csa(unsigned int index, const std::vector<Task> &task_vector, const std::vector<double> &resp_time_hp, double resp_time)
{
	double interference = 0;

	for (unsigned int i = 0; i < index; i++)
	{
		if (task_vector[i].getGe() != 0)
		{
			interference = interference + ceil((resp_time + resp_time_hp[i] - (task_vector[i].getC()+task_vector[i].getGm()))/task_vector[i].getT())*(task_vector[i].getC()+task_vector[i].getGm());	
		}
		else
			interference = interference + ceil((resp_time)/task_vector[i].getT())*(task_vector[i].getC());
	}
	return interference;
}

/**************** The CycleSolo-Accel Calculate High-Priority Interference sub-routine ********************/ 
double calculate_interference_sysclock_csa(unsigned int index, const std::vector<Task> &task_vector, const std::vector<double> &resp_time_hp, double resp_time)
{
	double interference = 0;

	for (unsigned int i = 0; i < index; i++)
	{
		if (task_vector[i].getGe() != 0)
			interference = interference + (floor((resp_time + resp_time_hp[i] - (task_vector[i].getC()+task_vector[i].getGm()))/task_vector[i].getT())+1)*(task_vector[i].getC()+task_vector[i].getGm());
		else
			interference = interference + (floor((resp_time)/task_vector[i].getT())+1)*(task_vector[i].getC());
	}
	return interference;
}

/**************** The CycleSolo-Accel Calculate High-Priority response time sub-routine ********************/ 
std::vector<double> calculate_hp_resp_time_csa(unsigned int index, const std::vector<Task> &task_vector, double f_est)
{
	double blocking, cpu_intervention;
	double resp_time, resp_time_dash, init_resp_time;
	std::vector<double> resp_time_hp(index, 0);

	for (unsigned int i = 0; i < index; i++)
	{
		blocking = calculate_blocking_csa(i, task_vector, f_est, resp_time_hp, &cpu_intervention);
		init_resp_time = ((task_vector[i].getC()+task_vector[i].getGm())) + (task_vector[i].getGe()/f_est) + blocking;
		resp_time = init_resp_time;
		resp_time_dash = 0;
		while (resp_time != resp_time_dash)
		{
			resp_time = resp_time_dash;
			resp_time_dash = init_resp_time + calculate_interference_scaled_csa(i, task_vector, resp_time_hp, resp_time);
		}
		resp_time_hp[i] = resp_time;
	}

	return resp_time_hp;
}

/**************** The CycleSolo-Accel Estimate Min Frequency sub-routine ********************/ 
int estimate_min_frequency_csa(unsigned int index, const std::vector<Task> &task_vector, double *f_up, double *f_low)
{
	double f_est = *f_up;
	double slack, idle, beta, delta, mu, t;
	double blocking, cpu_intervention, resp_time, resp_time_init, resp_time_dash;
	double interference;
	double gpu_exec;
	std::vector<double> resp_time_hp;
	int busy_flag, check_sched_flag = 0;

	double t_crit;

	// Variable initialization
	slack = 0;			// Slack Variable
	idle = 0;			// Idle Time
	beta = 0;			// Workload
	delta = 0;			// Change in resp time estimates
	mu = 1;				// Worload util
	busy_flag = 1;		// Processor busy flag

	// Calculate response time of high priority tasks
	resp_time_hp = calculate_hp_resp_time_csa(index, task_vector, f_est);

	// Calculate blocking of this task
	blocking = calculate_blocking_csa(index, task_vector, f_est, resp_time_hp, &cpu_intervention);

	// GPU execution
	gpu_exec = task_vector[index].getGe(); 

	// Initial estimate of response time
	resp_time = task_vector[index].getC() + task_vector[index].getGe() + task_vector[index].getGm() + blocking;
	resp_time_init = resp_time;

	// Frequency estimation loop
	while (resp_time < task_vector[index].getD())
	{
		if (busy_flag == 1)
		{
			delta = task_vector[index].getD() - resp_time;
			while (resp_time < task_vector[index].getD() && delta > 0)
			{
				resp_time_dash = calculate_interference_sysclock_csa(index, task_vector, resp_time_hp, resp_time);
				resp_time_dash = resp_time_dash + resp_time_init + slack;
				delta = resp_time_dash - resp_time;
				if (resp_time_dash > resp_time)
					resp_time = resp_time_dash;
			}

			// Check Task set schedulability in the first iteration 
			if(resp_time > task_vector[index].getD() && check_sched_flag == 0)
			{
				// if the f_est is 1 the task not schedulable at highest frequency
				if (f_est == 1)
					return -1;
				else
				{
					// Reset the frequency to 1 if a task is not schedulable at the max frequency
					f_est = 1;
					*f_up = 1;
					/* Recalculate all the starter variables and restart the estimation */
					// Calculate response time of high priority tasks
					resp_time_hp = calculate_hp_resp_time_csa(index, task_vector, f_est);

					// Calculate blocking of this task
					blocking = calculate_blocking_csa(index, task_vector, f_est, resp_time_hp, &cpu_intervention);

					// GPU execution
					gpu_exec = task_vector[index].getGe();

					// Initial estimate of response time
					resp_time = task_vector[index].getC() + task_vector[index].getGe() + task_vector[index].getGm() + blocking;
					resp_time_init = resp_time;
					continue;
				}
			} 

			check_sched_flag = 1;	// Task is schedulable if we reach here
			busy_flag = 0;
		}
		else
		{
		    
		    idle = find_min_idle_csa(index, task_vector, resp_time_hp, resp_time, task_vector[index].getD());
		    
		    // Recalibrate the Interference to match the new proposed response time
		    interference = calculate_interference_csa(index, task_vector, resp_time_hp, resp_time+idle);
		    delta = interference - calculate_interference_sysclock_csa(index, task_vector, resp_time_hp, resp_time);
		    if (idle - delta > EPSILON_FLO && delta >= EPSILON_FLO)
		    {
		    	idle = idle - delta;
		    	resp_time = resp_time + delta;
		    }
		    else if (delta >= idle)
		    {
		    	resp_time = resp_time + idle;
		    	idle = 0.000;
		    }

		    // Update Slack
			slack = slack + idle;
			resp_time = resp_time + idle;
			t = gpu_exec + slack;
			beta = gpu_exec;
			
			if (beta/t < mu)
			{
				mu = beta/t;
				t_crit = resp_time;
			}

			busy_flag = 1;
		}
	}
		
	// Reconfigure frequency for next estimation
	if (mu > *f_up) 
	{
		*f_low = *f_up;
		*f_up = mu;
	}
	else if (mu > *f_low)
	{
		*f_low = mu;
	}
	return 0;
}

/**************** The CycleSolo-Accel Algorithm ********************/ 
int cycle_solo_accel(const std::vector<Task> &task_vector, double *f_up, double *f_low)
{
	int retval;
	std::vector<double> min_frequency;
	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
    	// Estimate Minimum frequency at which task meets deadline
    	if (task_vector[index].getGe() != 0)
    	{
    		retval = estimate_min_frequency_csa(index, task_vector, f_up, f_low);
	    	if (retval < 0)
	    	{
	    		// Task set not schedulable
	    		return -1;
	    	}
	    }
	}
	return 0;
}