/*
 * @file cycle-solo.hpp
 * @brief CycleSolo Algorithms Header
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CYCLESOLO_HPP
#define CYCLESOLO_HPP

/* Internal Headers */
#include "task.hpp"

/**************** The CycleSolo-ID Algorithm ********************/ 
/* Params: task_vector: vector of tasks 
           f_up       : pointer to frequency upper bound (should be initialized)
           f_down     : pointer to frequency lower bound (should be initialized)
   Returns: 0 if schedulable */
int cycle_solo_id(const std::vector<Task> &task_vector, double *f_up, double *f_low);

/**************** The CycleSolo-CPU Algorithm ********************/ 
/* Params: task_vector: vector of tasks 
           f_up       : pointer to frequency upper bound (should be initialized)
           f_down     : pointer to frequency lower bound (should be initialized)
   Returns: 0 if schedulable */
int cycle_solo_cpu(const std::vector<Task> &task_vector, double *f_up, double *f_low);

/**************** The CycleSolo-Accel Algorithm ********************/ 
/* Params: task_vector: vector of tasks 
           f_up       : pointer to frequency upper bound (should be initialized)
           f_down     : pointer to frequency lower bound (should be initialized)
   Returns: 0 if schedulable */
int cycle_solo_accel(const std::vector<Task> &task_vector, double *f_up, double *f_low);

/**************** The Multi-core CycleSolo-ID Algorithm ********************/ 
/* Params: task_vector: vector of tasks 
           f_up       : pointer to frequency upper bound (should be initialized)
           f_down     : pointer to frequency lower bound (should be initialized)
   Returns: 0 if schedulable */
int cycle_solo_id_mc(const std::vector<Task> &task_vector, double *f_up, double *f_low);

/**************** The Multi-core CycleSolo-CPU Algorithm ********************/ 
/* Params: task_vector    : vector of tasks 
           f_up           : vector of per-core frequency upper bound (should be initialized)
           f_down         : vector of per-core frequency lower bound (should be initialized)
           sync_freq_flag : flag indicating if all cores share the same frequency
   Returns: 0 if schedulable */
int cycle_solo_cpu_mc(const std::vector<Task> &task_vector, std::vector<double> &f_up, std::vector<double> &f_low, int syncfreq_flag);

/**************** The Multi-core CycleSolo-Accel Algorithm ********************/ 
/* Params: task_vector: vector of tasks 
           f_up       : pointer to frequency upper bound (should be initialized)
           f_down     : pointer to frequency lower bound (should be initialized)
   Returns: 0 if schedulable */
int cycle_solo_accel_mc(const std::vector<Task> &task_vector, double *f_up, double *f_low);

/**************** The Multi-Core CycleSolo-CPU Algorithm for Independent Frequency per-core ********************/ 
/* Params: task_vector    : vector of tasks 
           f_cpu_per_core : vector of per-core frequency (is initialized by this function to the cycle solo CPU bounds)
           f_cpu_cs_solo  : frequency returned by cycle solo CPU multicore with all same frequency
           num_cores      : num
   Returns: 0 if schedulable */
int cycle_solo_cpu_mc_independent(const std::vector<Task> &task_vector, std::vector<double> &f_cpu_per_core, 
                                  double f_cpu_cs_solo, int num_cores, double epsilon);

#endif
