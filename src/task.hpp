/*
 * @file task.hpp
 * @brief Task class header
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TASK_HPP
#define TASK_HPP

#include <vector>

typedef struct task {
	double C;		// WCET on CPU	
	double Gm;		// WCET CPU intervention
	double Ge;		// WCET GPU execution
	double D;		// Deadline < Period
	double T;		// Period
} task_t;

class Task
{
	// Constructor and destructor
	public: Task(task_t task_params);
	public: ~Task();

	// Get the task parameters
	public: double getC() const; 
	public: double getGm() const; 
	public: double getGe() const; 
	public: double getD() const; 
	public: double getT() const;
	public: double getCpuFreq() const; 
	public: double getGpuFreq() const; 
	public: unsigned int getCoreID() const; 

	// Set the core to which the task is allocated
	public: int setCoreID(unsigned int coreID);

	// Scale the task parameters by the frequency scaling factor
	public: int scale_cpu(double cpu_frequency);
	public: int scale_gpu(double gpu_frequency);

	// Convert the timescale multiply and floor (to remove floating point errors)
	public: int task_timescale(int scaling_factor); 

	// Asynchronous service
	private: task_t params;
	private: double cpu_freq;
	private: double gpu_freq;
	private: int core_id; 
};

#endif
