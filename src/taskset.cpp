/*
 * @file taskset.cpp
 * @brief Taskset operations helper functions
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream> 

#include "taskset.hpp"

/**************** Print all the tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: 0 if schedulable */
void print_taskset(const std::vector<Task> &task_vector)
{
	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		std::cout << "Task " << index << 
		             ": C = " << task_vector[index].getC() <<
		             ", Ge = " << task_vector[index].getGe() <<
		             ", Gm = " << task_vector[index].getGm() <<
		             ", D = " << task_vector[index].getD() <<
		             ", T = " << task_vector[index].getT() << 
		             ", Core = " << task_vector[index].getCoreID() <<"\n";
	}
}

/**************** Scale the task set CPU & GPU frequency by the given factor ********************/ 
/* Params: task_vector: vector of tasks 
		   cpu_frequency: cpu frequency scaling factor
		   gpu_frequency: gpu frequency scaling factor
   Returns: new task vector */
std::vector<Task> scale_taskset_frequency(const std::vector<Task> &task_vector, double cpu_frequency, double gpu_frequency)
{
	std::vector<Task> scaled_task_vector;

	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		Task task = task_vector[index];
		task.scale_cpu(cpu_frequency);
		task.scale_gpu(gpu_frequency);
		scaled_task_vector.push_back(task);
	}
	return scaled_task_vector;
}

/**************** Scale the task set CPU (per-core) & GPU frequency by the given factor ********************/ 
/* Params: task_vector: vector of tasks 
		   cpu_frequency: per-core cpu frequency scaling factor
		   gpu_frequency: gpu frequency scaling factor
   Returns: new task vector */
std::vector<Task> scale_taskset_per_core_frequency(const std::vector<Task> &task_vector, const std::vector<double> &cpu_frequency, double gpu_frequency)
{
	std::vector<Task> scaled_task_vector;

	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		Task task = task_vector[index];
		task.scale_cpu(cpu_frequency[task.getCoreID()]);
		task.scale_gpu(gpu_frequency);
		scaled_task_vector.push_back(task);
	}
	return scaled_task_vector;
}


/**************** Get the CPU utilization all the tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: cpu utilization */
double get_taskset_cpu_util(const std::vector<Task> &task_vector)
{
	double cpu_util = 0.00;
	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		cpu_util = cpu_util + ((task_vector[index].getC() + task_vector[index].getGm())/task_vector[index].getT());
	}
	return cpu_util;
}

/**************** Get the CPU utilization the tasks in the taskset allocated to a given core *************************/
/* Params: task_vector: vector of tasks 
		   core_id: id of the core
   Returns: cpu utilization */
double get_taskset_cpu_util_per_core(const std::vector<Task> &task_vector, int core_id)
{
	double cpu_util = 0.00;
	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		if (core_id == task_vector[index].getCoreID())
		{
			cpu_util = cpu_util + ((task_vector[index].getC() + task_vector[index].getGm())/task_vector[index].getT());
		}
	}
	return cpu_util;
}

/**************** Get the CPU utilization GPU-using tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: cpu utilization of GPU-using tasks */
double get_gputasks_cpu_util(const std::vector<Task> &task_vector)
{
	double cpu_util = 0.00;
	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		if (task_vector[index].getGe() != 0)
			cpu_util = cpu_util + ((task_vector[index].getC() + task_vector[index].getGm())/task_vector[index].getT());
	}
	return cpu_util;
}

/**************** Get the GPU utilization all the tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: gpu utilization */
double get_taskset_gpu_util(const std::vector<Task> &task_vector)
{
	double gpu_util = 0.00;
	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		gpu_util = gpu_util + (task_vector[index].getGe()/task_vector[index].getT());
	}
	return gpu_util;
}

/* Find index of max lp task with largest GPU segment */
/* Params: index      : task index in vector ordered by priority
           task_vector: vector of tasks 
   Returns: index of low-priority task with max GPU segment */
int find_max_lp_gpu_index(int index, const std::vector<Task> &task_vector)
{
	int Gl_max_index = index + 1;
	double Gl_max = 0;
	double Gl;
	for (unsigned int i = index + 1; i < task_vector.size(); i++)
	{
		Gl = task_vector[i].getGe() + task_vector[i].getGm();
		if (Gl > Gl_max)
		{
			Gl_max = Gl;
			Gl_max_index = i;
		}
	}
	return Gl_max_index;
}
