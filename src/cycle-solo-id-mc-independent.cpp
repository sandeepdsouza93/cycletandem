/*
 * @file cycle-solo-cpu-mc-independent.cpp
 * @brief CycleSolo-CPU multi-core implementation with independent per-core frequency 
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* Standard Library Imports */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <chrono>

/* Internal Headers */
#include "task.hpp"
#include "taskset.hpp"
#include "request-driven-test.hpp"
#include "binary-search.hpp"
#include "config.hpp"

/**************** The Multi-Core CycleSolo-CPU Algorithm for Independent Frequency per-core ********************/ 
int find_lowest_core_frequencies(const std::vector<Task> &task_vector, std::vector<double> &f_cpu_per_core, 
	const std::vector<double> &min_frequency_per_core, const std::vector<int> &marked_cores, int num_cores, double epsilon)
{
	int best_core = -1;
	int most_energy_saved = 0;
	double best_frequency = 1;
	double cycle_solo_freq;
	double core_util;
	double energy_saved;
	double min_core_frequency_found;

	// Find the lowest frequency each core can go to, while keeping all the other cores set to f_cpu_per_core
	for (unsigned int core = 0; core < num_cores; core++)
	{
		// Check is the core is marked --> we have already optimized its frequency
		if (std::find(marked_cores.begin(), marked_cores.end(), core) != marked_cores.end())
			continue;

		// f_cpu_per_core[core] has the cycle solo frequency if this core is unmarked
		cycle_solo_freq = f_cpu_per_core[core];   // search range upper bound
		core_util = min_frequency_per_core[core]; // search range lower bound

		// Find the minimum feasible frequency for this core, setting all other cores to a fixed frequency
		min_core_frequency_found = binary_search_per_core_cpu_frequency_range(task_vector, epsilon, 1.0, cycle_solo_freq, core_util, f_cpu_per_core, core);

		// Find the extra energy saved
		energy_saved = core_util*(pow(cycle_solo_freq, 2.0) - pow(min_core_frequency_found, 2.0));

		// Update best parameters
		if (energy_saved >= most_energy_saved)
		{
			best_core = core;
			most_energy_saved = energy_saved;
			best_frequency = min_core_frequency_found;
		}

		// Return the perturbed core's frequency back to the original state
		f_cpu_per_core[core] = cycle_solo_freq;
	}

	// Set the best cores frequency to the best frequency (in terms of energy savings)
	f_cpu_per_core[best_core] = best_frequency;
	return best_core;
}

/**************** The Multi-Core CycleSolo-CPU Algorithm for Independent Frequency per-core ********************/ 
int cycle_solo_cpu_mc_independent(const std::vector<Task> &task_vector, std::vector<double> &f_cpu_per_core, double f_cpu_cs_solo, 
								 int num_cores, double epsilon)
{
	int retval;
	std::vector<double> min_frequency_per_core;			// The minimum frequency possible per core (core util)
	std::vector<int> marked_cores;						// The cores for which we have set the frequency
	int best_core = -1;

	// Create a vector of the minimum per-core frequency possible (cpu utilization per-core)
	min_frequency_per_core.resize(num_cores);
	for (unsigned int core = 0; core < num_cores; core++)
	{
		min_frequency_per_core[core] = get_taskset_cpu_util_per_core(task_vector, core);

		// Initialize the per-core frequency to the cyclesolo-CPU frequency (all cores are schedulable at this frequency)
		f_cpu_per_core[core] = f_cpu_cs_solo;
	}

	// Perform per-core optimization (we only need to optimize n-1 of n cores, nth core runs at f_cpu_cs_solo)
	for (unsigned int index = 0; index < num_cores - 1; index++) 
	{
		best_core = find_lowest_core_frequencies(task_vector, f_cpu_per_core, min_frequency_per_core, marked_cores, num_cores, epsilon);
		marked_cores.push_back(best_core);
	}

	if (DEBUG)
	{
		printf("CycleSolo-CPU Frequency %f\n", f_cpu_cs_solo);
		for (unsigned int core = 0; core < num_cores; core++)
		{
			printf("Core %d Frequency %f\n", core, f_cpu_per_core[core]);
		}
	}

	return 0;
}