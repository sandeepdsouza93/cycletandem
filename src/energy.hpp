/*
 * @file energy.hpp
 * @brief Header for the energy calculation function
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ENERGY_HPP
#define ENERGY_HPP

#include <vector>

/**************** Energy Calculation in the Hyperperiod ********************/ 
/* Params: cputime       : cpu time in the hyperperiod 
		       gputime       : gpu time in the hyperperiod
           cpu_frequency : cpu frequency scaling factor
           gpu_frequency : gpu frequency scaling factor	
   Returns: energy usage in the hyperperiod */
double calculate_energy(double cputime, double gputime, double cpu_frequency, double gpu_frequency);

/**************** Energy Calculation in the Hyperperiod for CPUs with independent per-core frequencies ********************/ 
/* Params: cputime       : vector of per-core cpu time in the hyperperiod 
           gputime       : gpu time in the hyperperiod
           cpu_frequency : vector of per-core cpu frequency scaling factor
           gpu_frequency : gpu frequency scaling factor 
           num_cores     : number of cpu cores
   Returns: energy usage in the hyperperiod */
double calculate_energy_independent(std::vector<double> &cputime, double gputime, 
                                    std::vector<double> &cpu_frequency, double gpu_frequency, int num_cores);

/**************** CPU Energy Calculation in the Hyperperiod ********************/ 
/* Params: cputime       : cpu time in the hyperperiod 
           cpu_frequency : cpu frequency scaling factor
   Returns: energy usage in the hyperperiod */
double calculate_cpu_energy(double cputime, double cpu_frequency);

/**************** GPU Energy Calculation in the Hyperperiod ********************/ 
/* Params: gputime       : gpu time in the hyperperiod
           gpu_frequency : gpu frequency scaling factor	
   Returns: energy usage in the hyperperiod */
double calculate_gpu_energy(double gputime, double gpu_frequency);


/**************** Get CPU energy constant ********************/ 
/* Params: cputime : cpu time in the hyperperiod 
   Returns: CPU energy constant */
double get_cpu_energy_constant(double cputime);


/**************** Get GPU energy constant ********************/ 
/* Params: cputime : cpu time in the hyperperiod 
   Returns: GPU energy constant */
double get_gpu_energy_constant(double gputime);

/**************** Set energy constants ********************/ 
/* Params: kcpu: CPU energy constant
		   kgpu: GPU energy constant
		   energyfactor: power to which the frequency is raised 
   Returns: 0 if succesful */
int set_energy_constants(double kcpu, double kgpu, double energyfactor);

#endif
