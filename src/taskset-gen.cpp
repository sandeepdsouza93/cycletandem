/*
 * @file taskset-gen.cpp
 * @brief Taskset generation implementation (UUniFast-discard)
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Include Stdlib Headers
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>

// Include internal headers
#include "taskset-gen.hpp"

/**************** The UUniFast-Discard Algorithm to generate taskset utilization numbers ********************/ 
/* Params: number_tasks: number of tasks
           utilization bound: taskset utilization desired
           task_upper_bound: the upper bound on a single task's utilization
           utilization_array: vector of generated utilization values */
int UUniFast(int number_tasks, double utilization_bound, double task_upper_bound, std::vector<double> &utilization_array)
{
	double sum;
	double next_sum;
	int i;
	double random;
	int found_flag;
	int unifast_iterations = 0;
	int terminate_iterations = 1000;	// Iterations to terminate UUniFast-Discard if any task has util > 1

	// Return -1 if the number of tasks are too less
	if (utilization_bound/number_tasks > task_upper_bound)
		return -1;

	for(unifast_iterations = 0; unifast_iterations< terminate_iterations; unifast_iterations++)
	{ 
		sum = utilization_bound;
		found_flag = 1;
		// Generate task utilization values
		for(i=1; i<number_tasks; i++)
		{
			random = (double)(rand() % 10000000)/(double)10000000;
			next_sum = sum*((double)pow(random, ((double)1/((double)(number_tasks - i)))));
			utilization_array[i-1] = sum - next_sum;
			if(utilization_array[i-1] > task_upper_bound)
			{
				found_flag = 0;
				break;
			}
			sum = next_sum;
		}
		// If a feasible taskset is found, generate the last task
		if(found_flag == 1)
		{
			utilization_array[i-1] = sum;
			if(utilization_array[i-1] <= task_upper_bound)
			{
				break;
			}
			else
			{
				found_flag = 0;
			}
		}
	}
	return -(1-found_flag);		// Returns 0 if found, -1 if not
}

/**************** Generate a random taskset ********************/ 
/* Params: number_tasks: number of tasks
		   number_gpu_tasks: number of tasks with gpu sections
           utilization bound: taskset utilization desired
           gpu_utilization_bound: taskset utilization gpu bound */
std::vector<Task> generate_tasks(int number_tasks, int number_gpu_tasks, double utilization_bound, double gpu_utilization_bound, int harmonic_flag)
{
	int min_period = MIN_PERIOD;					        // in ms
	int max_period = MAX_PERIOD;							// in ms
	double cpu_task_upper_bound = CPU_TASK_UPPER_BOUND;  
	double gpu_task_upper_bound = GPU_TASK_UPPER_BOUND;  
	double cpu_intervention_util = CPU_INTERVENTION_UTIL;
	double cpu_intervention_bound = CPU_INTERVENTION_BOUND;         
           
	int i = 0;
	int random;

	std::vector<double> utilization_array(number_tasks, 0.0);
	std::vector<double> gpu_utilization_array(number_gpu_tasks, 0.0);
	std::vector<Task> task_vector;
	task_t task_params;

	// Check if number of tasks with GPU sections is less than total number of tasks
	if (number_gpu_tasks > number_tasks || number_tasks <= 0)
		return task_vector;
	
	// Generate CPU utilization array using UUniFast-Discard
	if(UUniFast(number_tasks, utilization_bound, cpu_task_upper_bound, utilization_array))
	{
		return task_vector;
	}

	if (number_gpu_tasks > 0)
	{
		// Generate GPU utilization array using UUniFast-Discard
		if(UUniFast(number_gpu_tasks, gpu_utilization_bound, gpu_task_upper_bound, gpu_utilization_array))
		{
			return task_vector;
		}
	}

	while(i < number_tasks)
	{
		// Randomly Initialize Time Periods
		random = rand();
		if(harmonic_flag == 1 && i == 0)
		{
			task_params.T = (random % (min_period)) + min_period;
		}
		else if(harmonic_flag == 1 && i > 0)
		{
			task_params.T = ((random % (3)) + 1)*task_params.T;
		}
		else
		{
			task_params.T = (random % (max_period-min_period)) + min_period;
		}

		// Set the deadline to be implicit
		task_params.D = task_params.T;

		// Set the CPU computation time based on UUniFast
		task_params.C = utilization_array[i]*task_params.T;

		// Initialize the GPU computation time
		task_params.Gm = 0;
		task_params.Ge = 0;

		// Set the GPU computation time
		if (i < number_gpu_tasks)
		{
			task_params.Ge = gpu_utilization_array[i]*task_params.T;
			if (cpu_intervention_util*task_params.Ge < cpu_intervention_bound)
			{
				task_params.Gm = cpu_intervention_util*task_params.Ge;
				task_params.Ge = task_params.Ge - task_params.Gm;
			}
			else
			{
				task_params.Gm = cpu_intervention_bound;
				task_params.Ge = task_params.Ge - cpu_intervention_bound;
			}
		}

		// Create the task class
		Task task(task_params);
		task_vector.push_back(task);

		i++;
	}
	return task_vector;
}