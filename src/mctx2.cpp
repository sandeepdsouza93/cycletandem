/*
 * @file mctx2.cpp
 * @brief Multi-core TX2 Experimental Tasksets
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* Standard Library Imports */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <chrono>

/* Internal Headers */
#include "task.hpp"
#include "taskset.hpp"
#include "taskset-gen.hpp"
#include "request-driven-test.hpp"
#include "binary-search.hpp"
#include "config.hpp"
#include "energy.hpp"
#include "cycle-solo.hpp"
#include "cycle-tandem.hpp"
#include "task_partitioning.hpp"

// Comparator class for ordering tasks using RMS
struct CompareTaskPriorityRMS {
    bool operator()(Task const & t1, Task const & t2) {
        // return "true" if "t1" has higher RMS priority than "t2"
        return t1.getT() < t2.getT();
    }
} ComparePriorityRMS;


int main(int argc, char **argv)
{
	std::vector<Task> task_vector, scaled_task_vector;
	double best_frequency_csc, best_frequency_csa, best_frequency_csi;
	double best_cpufreq_ctg, best_gpufreq_ctg, best_cpufreq_cte, best_gpufreq_cte;
	double best_frequency_up, best_frequency_low;
	double energy_csc, energy_csa, energy_csi, energy_ctg, energy_ctb;
	double true_gpu_util, true_cpu_util;
	int number_tasks;
	int retval;
	int number_gpu_tasks;
	int sched_flag = 0;
	int post_sched_flag = 0;
	task_t tasks[20];
	int num_cores = 4; // The TX2 has 4 ARM Cortex A57 Cores

	std::vector<double> discrete_cpu_freq = {345600, 499200, 652800, 806400, 960000, 1113600, 1267200, 1420800, 1574400, 1728000, 1881600, 2035200}; 
	std::vector<double> discrete_gpu_freq = {114750000, 216750000, 318750000, 420750000, 522750000, 624750000, 726750000, 828750000, 930750000, 1032750000, 1134750000};

	// Epsilon convergence factor
	double epsilon = 0.01;
	if (argc > 1)
		epsilon = std::atof(argv[1]);

	// Energy Constants
	if (argc > 4)
	{
		/* Kcpu = 1, Kgpu = 1, Energy Factor = 2*/
		if (set_energy_constants(std::atof(argv[2]),std::atof(argv[3]),std::atof(argv[4])) < 0)
		{
			std::cout << "Negative energy constants not allowed\n";
			return -1;
		}
	}

	/* initialize random seed: */
  	srand (time(NULL));

  	/* Example Taskset1 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
	(20,100)=0.2   		(100->4)=0.04
	(10,100)=0.1		(150->6)=0.03
	(30,150)=0.2		(100->4)=0.0266
	(50,200)=0.25		(300->18)=0.09
	(50,300)=0.166		(200->9)=0.03
	(100,300)=0.33		(400->30)=0.1
	(100,600)=0.166		(250->13)=0.021
	(400,1200)=0.33		(400->30)=0.074
	(CPU Util, GPU Util) = (1.76, 0.39) */

	// tasks[0].C = 20; tasks[0].T = 100; tasks[0].D = tasks[0].T, tasks[0].Ge = 4, tasks[0].Gm = 0.3; 
	// tasks[1].C = 10; tasks[1].T = 100; tasks[1].D = tasks[1].T, tasks[1].Ge = 6, tasks[1].Gm = 0.3;
	// tasks[2].C = 30; tasks[2].T = 150; tasks[2].D = tasks[2].T, tasks[2].Ge = 4, tasks[2].Gm = 0.3;
	// tasks[3].C = 50; tasks[3].T = 200; tasks[3].D = tasks[3].T, tasks[3].Ge = 18, tasks[3].Gm = 0.3;
	// tasks[4].C = 50; tasks[4].T = 300; tasks[4].D = tasks[4].T, tasks[4].Ge = 9, tasks[4].Gm = 0.3;
	// tasks[5].C = 100; tasks[5].T = 300; tasks[5].D = tasks[5].T, tasks[5].Ge = 30, tasks[5].Gm = 0.3;
	// tasks[6].C = 100; tasks[6].T = 600; tasks[6].D = tasks[6].T, tasks[6].Ge = 13, tasks[6].Gm = 0.3;
	// tasks[7].C = 400; tasks[7].T = 1200; tasks[7].D = tasks[7].T, tasks[7].Ge = 30, tasks[7].Gm = 0.3;

	// Task tau_0(tasks[0]);		
	// Task tau_1(tasks[1]);
	// Task tau_2(tasks[2]);
	// Task tau_3(tasks[3]);
	// Task tau_4(tasks[4]);
	// Task tau_5(tasks[5]);
	// Task tau_6(tasks[6]);
	// Task tau_7(tasks[7]);

	// task_vector.push_back(tau_0);
	// task_vector.push_back(tau_1);
	// task_vector.push_back(tau_2);
	// task_vector.push_back(tau_3);
	// task_vector.push_back(tau_4);
	// task_vector.push_back(tau_5);
	// task_vector.push_back(tau_6);
	// task_vector.push_back(tau_7);

	/* # Example Taskset2 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
	(10,150)=0.06   		(250->13)=0.0866
	(50,300)=0.16		    (100->4)=0.0133
	(60,600)=0.1		    (300->18)=0.03
	(125,1200)=0.1  		(200->9)=0.0075
	(CPU Util, GPU Util) = (0.42, 0.14) */

	// tasks[0].C = 10; tasks[0].T = 150; tasks[0].D = tasks[0].T, tasks[0].Ge = 13, tasks[0].Gm = 0.3; 
	// tasks[1].C = 50; tasks[1].T = 300; tasks[1].D = tasks[1].T, tasks[1].Ge = 4, tasks[1].Gm = 0.3;
	// tasks[2].C = 60; tasks[2].T = 600; tasks[2].D = tasks[2].T, tasks[2].Ge = 18, tasks[2].Gm = 0.3;
	// tasks[3].C = 125; tasks[3].T = 1200; tasks[3].D = tasks[3].T, tasks[3].Ge = 9, tasks[3].Gm = 0.3;
	
	// Task tau_0(tasks[0]);		
	// Task tau_1(tasks[1]);
	// Task tau_2(tasks[2]);
	// Task tau_3(tasks[3]);

	// task_vector.push_back(tau_0);
	// task_vector.push_back(tau_1);
	// task_vector.push_back(tau_2);
	// task_vector.push_back(tau_3);

	// # Example Taskset3 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
	// (40,150)=0.27   	(100->4)=0.0266
	// (60,300)=0.2		(100->4)=0.0133
	// (200,450)=0.44  	(100->4)=0.008
	// (300,600)=0.5	(100->4)=0.0066
	// (400,900)=0.44  	(100->4)=0.0044
	// (1000,1800)=0.55	(100->4)=0.0022
	// (CPU Util, GPU Util) = (2.2, 0.052)

	// tasks[2].C = 200; tasks[2].T = 450; tasks[2].D = tasks[2].T, tasks[2].Ge = 4, tasks[2].Gm = 0.3;
	// tasks[3].C = 300; tasks[3].T = 600; tasks[3].D = tasks[3].T, tasks[3].Ge = 4, tasks[3].Gm = 0.3;
	// tasks[4].C = 400; tasks[4].T = 900; tasks[4].D = tasks[4].T, tasks[4].Ge = 4, tasks[4].Gm = 0.3;
	// tasks[5].C = 1000; tasks[5].T = 1800; tasks[5].D = tasks[5].T, tasks[5].Ge = 4, tasks[5].Gm = 0.3;
	// Task tau_2(tasks[2]);
	// Task tau_3(tasks[3]);
	// Task tau_4(tasks[4]);
	// Task tau_5(tasks[5]);
	
	// task_vector.push_back(tau_2);
	// task_vector.push_back(tau_3);
	// task_vector.push_back(tau_4);
	// task_vector.push_back(tau_5);

	// # Example Taskset4 (C,T) (Ge,Gm=0.3) -> All Matrix Multiplication Tasks
	// (2,250)=0.013   	(300->18)=0.072
	// (4,250)=0.026		(400->30)=0.12
	// (10,500)=0.02  	    (500->47)=0.094
	// (20,500)=0.04		(500->47)=0.094
	// (10,750)=0.013  	(700->89)=0.118
	// (10,1500)=0.002	    (400->30)=0.02
	// (CPU Util, GPU Util) = (0.1, 0.518) 

	tasks[0].C = 2; tasks[0].T = 250; tasks[0].D = tasks[0].T, tasks[0].Ge = 18, tasks[0].Gm = 0.3; 
	tasks[1].C = 4; tasks[1].T = 250; tasks[1].D = tasks[1].T, tasks[1].Ge = 30, tasks[1].Gm = 0.3;
	tasks[2].C = 10; tasks[2].T = 500; tasks[2].D = tasks[2].T, tasks[2].Ge = 47, tasks[2].Gm = 0.3;
	tasks[3].C = 20; tasks[3].T = 500; tasks[3].D = tasks[3].T, tasks[3].Ge = 47, tasks[3].Gm = 0.3;
	tasks[4].C = 10; tasks[4].T = 750; tasks[4].D = tasks[4].T, tasks[4].Ge = 89, tasks[4].Gm = 0.3;
	tasks[5].C = 10; tasks[5].T = 1500; tasks[5].D = tasks[5].T, tasks[5].Ge = 30, tasks[5].Gm = 0.3;

	Task tau_0(tasks[0]);		
	Task tau_1(tasks[1]);
	Task tau_2(tasks[2]);
	Task tau_3(tasks[3]);
	Task tau_4(tasks[4]);
	Task tau_5(tasks[5]);

	task_vector.push_back(tau_0);
	task_vector.push_back(tau_1);
	task_vector.push_back(tau_2);
	task_vector.push_back(tau_3);
	task_vector.push_back(tau_4);
	task_vector.push_back(tau_5);

	// Sort Vector based on Some Priority ordering (here RMS)
	std::sort(task_vector.begin(), task_vector.end(), ComparePriorityRMS);

	// Create a Partition
	sched_flag = worst_fit_decreasing(task_vector, num_cores, ComparePriorityRMS);

	print_taskset(task_vector);

	// If taskset is schedulable then compute frequency
	if (sched_flag == 0)
	{
		// Compute utilization values for energy calculations
		true_cpu_util = get_taskset_cpu_util(task_vector);
		true_gpu_util = get_taskset_gpu_util(task_vector);

		std::cout << "CPU Util   = " << true_cpu_util << "\n";
		std::cout << "Accel Util = " << true_gpu_util << "\n";

		// Initialize Variables
		std::vector<double> best_freq_up(num_cores, get_taskset_cpu_util(task_vector)/num_cores);
		std::vector<double> best_freq_low(num_cores, get_taskset_cpu_util(task_vector)/num_cores);

		// CycleSolo-CPU
		auto start_c = std::chrono::high_resolution_clock::now();
		retval = cycle_solo_cpu_mc(task_vector, best_freq_up, best_freq_low, 1);
		best_frequency_csc = binary_search_cpu_frequency_range(task_vector, epsilon, 1.0, best_freq_up[0], best_freq_low[0]);
		energy_csc = calculate_energy(true_cpu_util, true_gpu_util, best_frequency_csc, 1.0);
		auto finish_c = std::chrono::high_resolution_clock::now();
		std::cout << "CycleSolo-CPU: CPU Frequency = " << best_frequency_csc << "\n";

		// Initialize Variables
		best_frequency_up = get_taskset_gpu_util(task_vector);
		best_frequency_low = best_frequency_up;

		// CycleSolo-Accel
		start_c = std::chrono::high_resolution_clock::now();
		retval = cycle_solo_accel_mc(task_vector, &best_frequency_up, &best_frequency_low);
		best_frequency_csa = binary_search_gpu_frequency_range(task_vector, epsilon, 1.0, best_frequency_up, best_frequency_low);
		energy_csa = calculate_energy(true_cpu_util, true_gpu_util, 1.0, best_frequency_csa);
		finish_c = std::chrono::high_resolution_clock::now();
		std::cout << "CycleSolo-Accel: Accel Frequency = " << best_frequency_csa << "\n";

		// Initialize Variables
		best_frequency_up = get_taskset_cpu_util(task_vector)/num_cores;
		best_frequency_low = best_frequency_up;

		// CycleSolo-ID
		start_c = std::chrono::high_resolution_clock::now();
		retval = cycle_solo_id_mc(task_vector, &best_frequency_up, &best_frequency_low);
		best_frequency_csi = binary_search_common_frequency_range(task_vector, epsilon, best_frequency_up, best_frequency_low);
		energy_csi = calculate_energy(true_cpu_util, true_gpu_util, best_frequency_csi, best_frequency_csi);
		finish_c = std::chrono::high_resolution_clock::now();
		std::cout << "CycleSolo-ID: ID Frequency = " << best_frequency_csi << "\n";

		// Initialize Variables
		best_cpufreq_cte = best_frequency_csc;
		best_gpufreq_cte = best_frequency_csa;

		// CycleTandem Discrete Bruteforce Search
		start_c = std::chrono::high_resolution_clock::now();
		retval = cycle_tandem_bruteforce_discrete(task_vector, &best_cpufreq_cte, &best_gpufreq_cte, discrete_cpu_freq, discrete_gpu_freq, epsilon);
		energy_ctb = calculate_energy(true_cpu_util, true_gpu_util, best_cpufreq_cte, best_gpufreq_cte);
		finish_c = std::chrono::high_resolution_clock::now();
		std::cout << "CycleTandem-Brute: CPU Frequency = " << best_cpufreq_cte << " Accel Frequency = " << best_gpufreq_cte << "\n";

	}
	else
	{
		std::cout << "Not schedulable\n";
	}
	return 0;
}