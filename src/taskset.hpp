/*
 * @file taskset.hpp
 * @brief Taskset operations helper functions header
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TASKSET_HPP
#define TASKSET_HPP

#include <vector>

#include "task.hpp"

/**************** Print all the tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: 0 if schedulable */
void print_taskset(const std::vector<Task> &task_vector);

/**************** Scale the task set CPU & GPU frequency by the given factor ********************/ 
/* Params: task_vector: vector of tasks 
		   cpu_frequency: cpu frequency scaling factor
		   gpu_frequency: gpu frequency scaling factor
   Returns: new task vector */
std::vector<Task> scale_taskset_frequency(const std::vector<Task> &task_vector, double cpu_frequency, double gpu_frequency);

/**************** Scale the task set CPU (per-core) & GPU frequency by the given factor ********************/ 
/* Params: task_vector: vector of tasks 
		   cpu_frequency: per-core cpu frequency scaling factor
		   gpu_frequency: gpu frequency scaling factor
   Returns: new task vector */
std::vector<Task> scale_taskset_per_core_frequency(const std::vector<Task> &task_vector, const std::vector<double> &cpu_frequency, double gpu_frequency);

/**************** Get the CPU utilization all the tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: cpu utilization */
double get_taskset_cpu_util(const std::vector<Task> &task_vector);

/**************** Get the CPU utilization the tasks in the taskset allocated to a given core *************************/
/* Params: task_vector: vector of tasks 
		   core_id: id of the core
   Returns: cpu utilization */
double get_taskset_cpu_util_per_core(const std::vector<Task> &task_vector, int core_id);

/**************** Get the CPU utilization GPU-using tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: cpu utilization of GPU-using tasks */
double get_gputasks_cpu_util(const std::vector<Task> &task_vector);

/**************** Get the GPU utilization all the tasks in the taskset *************************/
/* Params: task_vector: vector of tasks 
   Returns: gpu utilization */
double get_taskset_gpu_util(const std::vector<Task> &task_vector);

/* Find index of max lp task with largest GPU segment */
/* Params: index      : task index in vector ordered by priority
           task_vector: vector of tasks 
   Returns: index of low-priority task with max GPU segment */
int find_max_lp_gpu_index(int index, const std::vector<Task> &task_vector);

#endif
