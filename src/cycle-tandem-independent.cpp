/*
 * @file cycle-tandem.cpp
 * @brief CycleTandem implementation
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* Standard Library Imports */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>

/* Internal Headers */
#include "config.hpp"
#include "task.hpp"
#include "taskset.hpp"
#include "taskset-gen.hpp"
#include "request-driven-test.hpp"
#include "binary-search.hpp"
#include "hyperperiod.hpp"
#include "energy.hpp"
#include "cycle-solo.hpp"


/**************** The CycleTandem Greedy Search GPU Algorithm ********************/ 
double cycle_tandem_greedy_search_gpu_independent(const std::vector<Task> &task_vector, double f_up, double f_low, 
												  std::vector<double> &cyclesolo_cpufreq, 
												  std::vector<double> &per_core_cpu_util, double gpu_util, 
												  int num_cores, double stepsize)
{
	double prev_energy = 100000000;
	double energy;
	double chosen_gpufreq = f_low;
	double f_cpu_cs_solo = cyclesolo_cpufreq[0];
	std::vector<double> calculated_cpu_frequencies(cyclesolo_cpufreq);
	std::vector<Task> gpu_scaled_taskset;

	for (double GPUFreq = f_low; GPUFreq <= f_up; GPUFreq = GPUFreq + stepsize)
	{
		// Scale taskset to GPU Frequency	
		gpu_scaled_taskset = scale_taskset_frequency(task_vector, 1.0, GPUFreq);

		// Perform CycleSolo Independent to find the frequencies on this new taskset
		cycle_solo_cpu_mc_independent(gpu_scaled_taskset, cyclesolo_cpufreq, f_cpu_cs_solo, num_cores, stepsize);

		// Calculate Energy
		energy = calculate_energy_independent(per_core_cpu_util, gpu_util, calculated_cpu_frequencies, GPUFreq, num_cores);
		
		// Break when a local minima is found
		if (energy > prev_energy)
		{
			break;	
		}

		prev_energy = energy;
		chosen_gpufreq = GPUFreq;
		cyclesolo_cpufreq = calculated_cpu_frequencies;
	}
	return chosen_gpufreq;
}

/**************** The CycleTandem Brute-Force Search GPU Algorithm with Independent CPU core frequencies ********************/ 
double cycle_tandem_bruteforce_search_gpu_independent(const std::vector<Task> &task_vector, double f_up, double f_low, 
												  std::vector<double> &cyclesolo_cpufreq, 
												  std::vector<double> &per_core_cpu_util, double gpu_util, 
												  int num_cores, double stepsize)
{
	double min_energy = 100000000;
	double energy;
	double chosen_gpufreq = f_low;
	double f_cpu_cs_solo = cyclesolo_cpufreq[0];
	std::vector<double> calculated_cpu_frequencies(cyclesolo_cpufreq);
	std::vector<Task> gpu_scaled_taskset;
	
	for (double GPUFreq = f_low; GPUFreq <= f_up; GPUFreq = GPUFreq + stepsize)
	{
		// Scale taskset to GPU Frequency	
		gpu_scaled_taskset = scale_taskset_frequency(task_vector, 1.0, GPUFreq);

		// Perform CycleSolo Independent to find the frequencies on this new taskset
		cycle_solo_cpu_mc_independent(gpu_scaled_taskset, cyclesolo_cpufreq, f_cpu_cs_solo, num_cores, stepsize);

		// Calculate Energy
		energy = calculate_energy_independent(per_core_cpu_util, gpu_util, calculated_cpu_frequencies, GPUFreq, num_cores);
		
		// Break when a local minima is found
		if (energy < min_energy)
		{
			min_energy = energy;
			chosen_gpufreq = GPUFreq;
			cyclesolo_cpufreq = calculated_cpu_frequencies;
		}
	}

	return chosen_gpufreq;
}

/**************** CycleTandem-Independent using Greedy Search ********************/ 
int cycle_tandem_independent(const std::vector<Task> &task_vector, std::vector<double> &cyclesolo_cpufreq, 
										double* cyclesolo_gpufreq, int num_cores, double epsilon)
{
	double gpufreq_up = 1;
	double gpufreq_chosen;
	std::vector<double> per_core_cpu_util;

	// Compute per-core cpu util
	per_core_cpu_util.resize(num_cores);
	for (unsigned int index = 0; index < num_cores; index++)
		per_core_cpu_util[index] = get_taskset_cpu_util_per_core(task_vector, index);

	// Get the GPU Util
	double gpu_util = get_taskset_gpu_util(task_vector);

	// Find the best frequencies
	gpufreq_chosen = cycle_tandem_greedy_search_gpu_independent(task_vector, gpufreq_up, *cyclesolo_gpufreq, cyclesolo_cpufreq, 
																	per_core_cpu_util, gpu_util, num_cores, epsilon);
	
	// Return the best values in the pointers
	*cyclesolo_gpufreq = gpufreq_chosen;
	return 0;
}


/**************** CycleTandem-Independent using Bruteforce Search ********************/ 
int cycle_tandem_independent_bruteforce(const std::vector<Task> &task_vector, std::vector<double> &cyclesolo_cpufreq, 
										double* cyclesolo_gpufreq, int num_cores, double epsilon)
{
	double gpufreq_up = 1;
	double gpufreq_chosen;
	std::vector<double> per_core_cpu_util;

	// Compute per-core cpu util
	per_core_cpu_util.resize(num_cores);
	for (unsigned int index = 0; index < num_cores; index++)
		per_core_cpu_util[index] = get_taskset_cpu_util_per_core(task_vector, index);

	// Get the GPU Util
	double gpu_util = get_taskset_gpu_util(task_vector);

	// Find the best frequencies
	gpufreq_chosen = cycle_tandem_bruteforce_search_gpu_independent(task_vector, gpufreq_up, *cyclesolo_gpufreq, cyclesolo_cpufreq, 
																	per_core_cpu_util, gpu_util, num_cores, epsilon);
	
	// Return the best values in the pointers
	*cyclesolo_gpufreq = gpufreq_chosen;
	return 0;
}