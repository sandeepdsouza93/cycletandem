/*
 * @file energy.cpp
 * @brief Energy calculation function
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cmath>

#include "energy.hpp"
#include "config.hpp"

// Initialize default energy constants
double k_cpu = K_CPU;
double k_gpu = K_GPU;
double energy_factor = ENERGY_FACTOR;

/**************** Energy Calculation in the Hyperperiod ********************/ 
double calculate_energy(double cputime, double gputime, double cpu_frequency, double gpu_frequency)
{
	double energy;
	energy = k_cpu*cputime*pow(cpu_frequency, energy_factor) + k_gpu*gputime*pow(gpu_frequency, energy_factor);
	return energy;
}

/**************** Energy Calculation in the Hyperperiod for CPUs with independent per-core frequencies ********************/ 
double calculate_energy_independent(std::vector<double> &cputime, double gputime, 
									std::vector<double> &cpu_frequency, double gpu_frequency, int num_cores)
{
	double energy = 0;
	
	// Find the CPU energy consumption per-core
	for (unsigned int core = 0; core < num_cores; core++)
		energy += k_cpu*cputime[core]*pow(cpu_frequency[core], energy_factor);

	// Add the GPU energy
	energy = energy + k_gpu*gputime*pow(gpu_frequency, energy_factor);
	return energy;
}


/**************** CPU Energy Calculation in the Hyperperiod ********************/ 
double calculate_cpu_energy(double cputime, double cpu_frequency)
{
	double energy;
	energy = k_cpu*cputime*pow(cpu_frequency, energy_factor);
	return energy;
}

/**************** GPU Energy Calculation in the Hyperperiod ********************/ 
double calculate_gpu_energy(double gputime, double gpu_frequency)
{
	double energy;
	energy = k_gpu*gputime*pow(gpu_frequency, energy_factor);
	return energy;
}

/**************** Get CPU energy constant ********************/ 
double get_cpu_energy_constant(double cputime)
{
	return k_cpu*cputime;
}


/**************** Get GPU energy constant ********************/ 
double get_gpu_energy_constant(double gputime)
{
	return k_gpu*gputime;
}

/**************** Set energy constant ********************/ 
int set_energy_constants(double kcpu, double kgpu, double energyfactor)
{
	if (kcpu < 0 || kgpu < 0 || energyfactor < 0)
		return -1;

	// Set the constants
	k_cpu = kcpu;
	k_gpu = kgpu;
	energy_factor = energyfactor;
	return 0;
}

