/*
 * @file request-driven-test.cpp
 * @brief Implementing the request-driven schedulability test (for single GPU segment per task)
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* Standard Library Imports */
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>

/* Internal Headers */
#include "request-driven-test.hpp"

/* Find index of max lp task with largest GPU segment */
int find_max_lp_gpu_index_rd(int index, const std::vector<Task> &task_vector)
{
	int Gl_max_index = index + 1;
	double Gl_max = 0;
	double Gl;
	for (unsigned int i = index + 1; i < task_vector.size(); i++)
	{
		Gl = task_vector[i].getGe() + task_vector[i].getGm();
		if (Gl > Gl_max)
		{
			Gl_max = Gl;
			Gl_max_index = i;
		}
	}
	return Gl_max_index;
}

/**************** Calculate Blocking sub-routine ********************/ 
double calculate_blocking_rd(unsigned int index, const std::vector<Task> &task_vector, const std::vector<double> &resp_time_hp)
{
	double blocking, Gl_max, blocking_dash;
	/* Find index of maximum low priority GPU segment */
	int Gl_max_index = find_max_lp_gpu_index_rd(index, task_vector);	

	// Return 0 blocking if task has no GPU execution
	if(task_vector[index].getGe() == 0)
		return 0;

	/* Note: Under this analysis even low-priority GPU access 
	         faces blocking from high-priority tasks */
	/* Refactor the maximum low priority blocking to take into account the CPU frequency */
	if (Gl_max_index < task_vector.size())
	{
		blocking = task_vector[Gl_max_index].getGe() + task_vector[Gl_max_index].getGm();
		Gl_max = blocking;
		// Ensure the initial value of blocking_dash is different from blocking
		if (Gl_max == 0)
		{
			blocking_dash = 1;
		}
		else
		{
			blocking_dash = 0;
		}
	}
	else
	{
		// No other lower priority tasks exist -> Vector Index has overrun
		blocking = 0;
		Gl_max = 0;
		// Ensure the initial value of blocking_dash is different from blocking
		blocking_dash = 1;
	}
	
	// Estimate Blocking
	while (blocking != blocking_dash)
	{
		blocking_dash = blocking;
		blocking = Gl_max;
		for (unsigned int i = 0; i < index; i++)
		{
			if (task_vector[i].getGe() != 0)
			{
				blocking = blocking 
			    + ceil((blocking_dash + resp_time_hp[i] - ((task_vector[i].getC()+task_vector[i].getGm())))/task_vector[i].getT())*(task_vector[i].getGe()+(task_vector[i].getGm()));
			}
		}
	}
	return blocking;
}

/**************** Calculate High-Priority Interference sub-routine ********************/ 
double calculate_interference_rd(unsigned int index, const std::vector<Task> &task_vector, const std::vector<double> &resp_time_hp, double resp_time)
{
	double interference = 0;
	unsigned int coreID = task_vector[index].getCoreID();

	for (unsigned int i = 0; i < index; i++)
	{
		if (task_vector[i].getCoreID() != coreID)
			continue;

		if (task_vector[i].getGe() != 0)
			interference = interference + ceil((resp_time + resp_time_hp[i] - ((task_vector[i].getC()+task_vector[i].getGm())))/task_vector[i].getT())*(task_vector[i].getC()+task_vector[i].getGm());
		else
			interference = interference + ceil((resp_time)/task_vector[i].getT())*(task_vector[i].getC());
	}
	return interference;
}

/**************** The CycleSolo-CPU Calculate High-Priority response time sub-routine ********************/ 
std::vector<double> calculate_hp_resp_time_rd(unsigned int index, const std::vector<Task> &task_vector)
{
	double blocking;
	double resp_time, resp_time_dash, init_resp_time;
	std::vector<double> resp_time_hp(index, 0);

	for (unsigned int i = 0; i < index; i++)
	{
		blocking = calculate_blocking_rd(i, task_vector, resp_time_hp);
		init_resp_time = ((task_vector[i].getC()+task_vector[i].getGm())) + task_vector[i].getGe() + blocking;
		resp_time = init_resp_time;
		resp_time_dash = 0;
		while (resp_time != resp_time_dash)
		{
			resp_time = resp_time_dash;
			resp_time_dash = init_resp_time + calculate_interference_rd(i, task_vector, resp_time_hp, resp_time);
		}
		resp_time_hp[i] = resp_time;
	}

	return resp_time_hp;
}

/**************** Calculate Schedulability using the Request-Driven Approach ********************/ 
int check_schedulability_request_driven(const std::vector<Task> &task_vector)
{
	std::vector<double> resp_time = calculate_hp_resp_time_rd(task_vector.size(), task_vector);

	for (unsigned int index = 0; index < task_vector.size(); index++) 
	{
		if (resp_time[index] <= task_vector[index].getD())
		{
			if (DEBUG)
				printf("Task %d schedulable, response time = %f\n", index, resp_time[index]);
		}
		else
		{
			if (DEBUG)
				printf("Task %d not schedulable, response time = %f\n", index, resp_time[index]);
			return -1;
		}
	}
	return 0;
}

