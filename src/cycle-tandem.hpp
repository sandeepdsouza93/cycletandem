/*
 * @file cycle-tandem.hpp
 * @brief CycleTandem Header
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CYCLETANDEM_HPP
#define CYCLETANDEM_HPP

#include <vector>

#include "task.hpp"

/**************** CycleTandem using Greedy Search ********************/ 
/* Params: task_vector       : vector of tasks 
           cyclesolo_cpufreq : pointer to cyclesolo cpu frequency, returns the chosen cpu frequency (should be initialized)
           cyclesolo_gpufreq : pointer to cyclesolo gpu frequency, returns the chosen gpu frequency (should be initialized)
           epsilon           : search step size
   Returns: 0 if schedulable */
int cycle_tandem(const std::vector<Task> &task_vector, double* cyclesolo_cpufreq, double* cyclesolo_gpufreq, double epsilon);


/**************** CycleTandem using Bruteforce Search ********************/ 
/* Params: task_vector       : vector of tasks 
           cyclesolo_cpufreq : pointer to cyclesolo cpu frequency, returns the chosen cpu frequency (should be initialized)
           cyclesolo_gpufreq : pointer to cyclesolo gpu frequency, returns the chosen gpu frequency (should be initialized)
           epsilon           : search step size
   Returns: 0 if schedulable */
int cycle_tandem_bruteforce(const std::vector<Task> &task_vector, double* cyclesolo_cpufreq, double* cyclesolo_gpufreq, double epsilon);

/**************** CycleTandem-Independent using Greedy Search ********************/ 
/* Params: task_vector       : vector of tasks 
           cyclesolo_cpufreq : vector of cyclesolo cpu frequency (assuming all cores are same), returns the chosen cpu frequency (should be initialized)
           cyclesolo_gpufreq : pointer to cyclesolo gpu frequency, returns the chosen gpu frequency (should be initialized)
           num_cores         : number of cores
           epsilon           : search step size
   Returns: 0 if schedulable */
int cycle_tandem_independent(const std::vector<Task> &task_vector, std::vector<double> &cyclesolo_cpufreq, 
                             double* cyclesolo_gpufreq, int num_cores, double epsilon);

/**************** CycleTandem-Independent using Bruteforce Search ********************/ 
/* Params: task_vector       : vector of tasks 
           cyclesolo_cpufreq : vector of cyclesolo cpu frequency(assuming all cores are same), returns the chosen cpu frequency (should be initialized)
           cyclesolo_gpufreq : pointer to cyclesolo gpu frequency, returns the chosen gpu frequency (should be initialized)
           num_cores         : number of cores 
           epsilon           : search step size
   Returns: 0 if schedulable */
int cycle_tandem_independent_bruteforce(const std::vector<Task> &task_vector, std::vector<double> &cyclesolo_cpufreq, 

                                        double* cyclesolo_gpufreq, int num_cores, double epsilon);
/**************** Discrete CycleTandem using Bruteforce Search ********************/ 
/* Params: task_vector       : vector of tasks 
           cyclesolo_cpufreq : pointer to cyclesolo cpu frequency, returns the chosen cpu frequency (should be initialized)
           cyclesolo_gpufreq : pointer to cyclesolo gpu frequency, returns the chosen gpu frequency (should be initialized)
		   discrete_cpu_freq : vector of available cpu frequencies
		   discrete_gpu_freq : vector of available gpu frequencies
		   epsilon           : search step size
   Returns: 0 if schedulable */
int cycle_tandem_bruteforce_discrete(const std::vector<Task> &task_vector, double* cyclesolo_cpufreq, double* cyclesolo_gpufreq, std::vector<double> &discrete_cpu_freq, std::vector<double> &discrete_gpu_freq, double epsilon);

#endif