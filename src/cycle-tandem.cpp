/*
 * @file cycle-tandem.cpp
 * @brief CycleTandem implementation
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* Standard Library Imports */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdlib>

/* Internal Headers */
#include "config.hpp"
#include "task.hpp"
#include "taskset.hpp"
#include "taskset-gen.hpp"
#include "request-driven-test.hpp"
#include "binary-search.hpp"
#include "hyperperiod.hpp"
#include "energy.hpp"

/**************** The CycleTandem Greedy Search CPU Algorithm ********************/ 
double cycle_tandem_greedy_search_cpu(const std::vector<Task> &task_vector, double f_up, double f_low, double stepsize)
{
	double prev_energy = 100000000;
	double energy;
	double chosen_cpufreq = f_low;

	double cpu_util = get_taskset_cpu_util(task_vector);
	double gpu_util = get_taskset_gpu_util(task_vector);

	for (double CPUFreq = f_low; CPUFreq <= f_up; CPUFreq = CPUFreq + stepsize)
	{
		// Get the best GPU frequency (assuming CPU frequency not scalable)
		double GPUFreq = binary_search_gpu_frequency(task_vector, stepsize, CPUFreq);
		energy = calculate_energy(cpu_util, gpu_util, CPUFreq, GPUFreq); 
		if (energy > prev_energy)
		{
			break;	// Break when a local minima is found
		}
		prev_energy = energy;
		chosen_cpufreq = CPUFreq;
	}
	return chosen_cpufreq;
}

/**************** The CycleTandem Greedy Search GPU Algorithm ********************/ 
double cycle_tandem_greedy_search_gpu(const std::vector<Task> &task_vector, double f_up, double f_low, double stepsize)
{
	double prev_energy = 100000000;
	double energy;
	double chosen_gpufreq = f_low;

	double cpu_util = get_taskset_cpu_util(task_vector);
	double gpu_util = get_taskset_gpu_util(task_vector);

	for (double GPUFreq = f_low; GPUFreq <= f_up; GPUFreq = GPUFreq + stepsize)
	{
		// Get the best GPU frequency (assuming CPU frequency not scalable)
		double CPUFreq = binary_search_cpu_frequency(task_vector, stepsize, GPUFreq);
		energy = calculate_energy(cpu_util, gpu_util, CPUFreq, GPUFreq); 
		if (energy > prev_energy)
		{
			break;	// Break when a local minima is found
		}
		prev_energy = energy;
		chosen_gpufreq = GPUFreq;
	}
	return chosen_gpufreq;
}

/**************** The CycleTandem Brute-Force Search CPU Algorithm ********************/ 
double cycle_tandem_bruteforce_search_cpu(const std::vector<Task> &task_vector, double f_up, double f_low, double stepsize)
{
	double min_energy = 100000000;
	double energy;
	double chosen_cpufreq = f_low;

	double cpu_util = get_taskset_cpu_util(task_vector);
	double gpu_util = get_taskset_gpu_util(task_vector);

	for (double CPUFreq = f_low; CPUFreq <= f_up; CPUFreq = CPUFreq + stepsize)
	{
		// Get the best GPU frequency (assuming CPU frequency not scalable)
		double GPUFreq = binary_search_gpu_frequency(task_vector, stepsize, CPUFreq);
		energy = calculate_energy(cpu_util, gpu_util, CPUFreq, GPUFreq); 
		if (energy < min_energy)
		{
			min_energy = energy;
			chosen_cpufreq = CPUFreq;
		}
	}
	return chosen_cpufreq;
}

/**************** The CycleTandem Brute-Force Search GPU Algorithm ********************/ 
double cycle_tandem_bruteforce_search_gpu(const std::vector<Task> &task_vector, double f_up, double f_low, double stepsize)
{
	double min_energy = 100000000;
	double energy;
	double chosen_gpufreq = f_low;

	double cpu_util = get_taskset_cpu_util(task_vector);
	double gpu_util = get_taskset_gpu_util(task_vector);

	for (double GPUFreq = f_low; GPUFreq <= f_up; GPUFreq = GPUFreq + stepsize)
	{
		// Get the best GPU frequency (assuming CPU frequency not scalable)
		double CPUFreq = binary_search_cpu_frequency(task_vector, stepsize, GPUFreq);
		energy = calculate_energy(cpu_util, gpu_util, CPUFreq, GPUFreq); 
		if (energy < min_energy)
		{
			min_energy = energy;
			chosen_gpufreq = GPUFreq;
		}
	}
	return chosen_gpufreq;
}

/**************** Compute Upper Bound on Optimal CPU Frequency ********************/ 
double compute_upper_bound_cpu(const std::vector<Task> &task_vector, double epsilon, double cyclesolo_gpufreq)
{
	if (cyclesolo_gpufreq <= 0)
		return -1;
	return binary_search_cpu_frequency(task_vector, epsilon, cyclesolo_gpufreq);
}

/**************** Compute Upper Bound on Optimal GPU Frequency ********************/ 
double compute_upper_bound_gpu(const std::vector<Task> &task_vector, double epsilon, double cyclesolo_cpufreq)
{
	if (cyclesolo_cpufreq <= 0)
		return -1;
	return binary_search_gpu_frequency(task_vector, epsilon, cyclesolo_cpufreq);
}

/**************** CycleTandem using Greedy Search ********************/ 
int cycle_tandem(const std::vector<Task> &task_vector, double* cyclesolo_cpufreq, double* cyclesolo_gpufreq, double epsilon)
{
	double cpufreq_up = 1; //compute_upper_bound_cpu(task_vector, epsilon, *cyclesolo_gpufreq);
	double gpufreq_up = 1; //compute_upper_bound_gpu(task_vector, epsilon, *cyclesolo_cpufreq);
	
	// Pre-Process the data to take care of cases where the GPU frequency is 0 (no GPU components) 
	if (*cyclesolo_cpufreq <= 0)
		gpufreq_up = *cyclesolo_gpufreq;

	if (*cyclesolo_gpufreq <= 0)
		cpufreq_up = *cyclesolo_cpufreq;


	double cpu_range = cpufreq_up - *cyclesolo_cpufreq;
	double gpu_range = gpufreq_up - *cyclesolo_gpufreq;

	double cpufreq_chosen, gpufreq_chosen;

	if (cpu_range <= gpu_range)
	{
		cpufreq_chosen = cycle_tandem_greedy_search_cpu(task_vector, cpufreq_up, *cyclesolo_cpufreq, epsilon);
		gpufreq_chosen = binary_search_gpu_frequency(task_vector, epsilon, cpufreq_chosen);
	}
	else
	{
		gpufreq_chosen = cycle_tandem_greedy_search_gpu(task_vector, gpufreq_up, *cyclesolo_gpufreq, epsilon);
		cpufreq_chosen = binary_search_cpu_frequency(task_vector, epsilon, gpufreq_chosen);
	}

	// Return the best values in the two pointers
	*cyclesolo_gpufreq = gpufreq_chosen;
	*cyclesolo_cpufreq = cpufreq_chosen;
	return 0;
}

/**************** CycleTandem using Bruteforce Search ********************/ 
int cycle_tandem_bruteforce(const std::vector<Task> &task_vector, double* cyclesolo_cpufreq, double* cyclesolo_gpufreq, double epsilon)
{
	double cpufreq_up = 1;//compute_upper_bound_cpu(task_vector, epsilon, *cyclesolo_gpufreq);
	double gpufreq_up = 1;//compute_upper_bound_gpu(task_vector, epsilon, *cyclesolo_cpufreq);

	double cpu_range = cpufreq_up - *cyclesolo_cpufreq;
	double gpu_range = gpufreq_up - *cyclesolo_gpufreq;

	double cpufreq_chosen, gpufreq_chosen;

	if (cpu_range < gpu_range)
	{
		cpufreq_chosen = cycle_tandem_bruteforce_search_cpu(task_vector, cpufreq_up, *cyclesolo_cpufreq, epsilon);
		gpufreq_chosen = binary_search_gpu_frequency(task_vector, epsilon, cpufreq_chosen);
	}
	else
	{
		gpufreq_chosen = cycle_tandem_bruteforce_search_gpu(task_vector, gpufreq_up, *cyclesolo_gpufreq, epsilon);
		cpufreq_chosen = binary_search_cpu_frequency(task_vector, epsilon, gpufreq_chosen);
	}

	// Return the best values in the two pointers
	*cyclesolo_gpufreq = gpufreq_chosen;
	*cyclesolo_cpufreq = cpufreq_chosen;
	return 0;
}

/* Get the next biggest discrete frequency */
double get_discrete_frequency(std::vector<double> &discrete_freq, double freq)
{
	for (unsigned int index = 0; index < discrete_freq.size(); index++)
	{
		if (discrete_freq[index]/discrete_freq[discrete_freq.size()-1] < freq)
			continue;
		else
			return discrete_freq[index]/discrete_freq[discrete_freq.size()-1];
	}
	return 1;
}

/**************** Discrete CycleTandem using Bruteforce Search ********************/ 
int cycle_tandem_bruteforce_discrete(const std::vector<Task> &task_vector, double* cyclesolo_cpufreq, double* cyclesolo_gpufreq, std::vector<double> &discrete_cpu_freq, std::vector<double> &discrete_gpu_freq, double epsilon)
{
	double cpufreq_chosen, gpufreq_chosen;
	double cpufreq_best = 1, gpufreq_best = 1; 

	double cpu_util = get_taskset_cpu_util(task_vector);
	double gpu_util = get_taskset_gpu_util(task_vector);

	double min_energy = 100000000;
	double energy;

	double max_cpufreq = discrete_cpu_freq[discrete_cpu_freq.size()-1];
	double max_gpufreq = discrete_gpu_freq[discrete_gpu_freq.size()-1];
	
	for (unsigned int index = 0; index < discrete_cpu_freq.size(); index++)
	{
		if (discrete_cpu_freq[index]/max_cpufreq < *cyclesolo_cpufreq)
			continue;
		
		cpufreq_chosen = discrete_cpu_freq[index]/max_cpufreq;
		gpufreq_chosen = binary_search_gpu_frequency(task_vector, epsilon, cpufreq_chosen);
		gpufreq_chosen = get_discrete_frequency(discrete_gpu_freq, gpufreq_chosen);

		energy = calculate_energy(cpu_util, gpu_util, cpufreq_chosen, gpufreq_chosen); 
		if (energy < min_energy)
		{
			min_energy = energy;
			cpufreq_best = cpufreq_chosen;
			gpufreq_best = gpufreq_chosen;
		}
	}

	for (unsigned int index = 0; index < discrete_gpu_freq.size(); index++)
	{
		if (discrete_gpu_freq[index]/max_gpufreq < *cyclesolo_gpufreq)
			continue;

		gpufreq_chosen = discrete_gpu_freq[index]/max_gpufreq;
		cpufreq_chosen = binary_search_cpu_frequency(task_vector, epsilon, gpufreq_chosen);
		cpufreq_chosen = get_discrete_frequency(discrete_cpu_freq, cpufreq_chosen);

		energy = calculate_energy(cpu_util, gpu_util, cpufreq_chosen, gpufreq_chosen); 
		if (energy < min_energy)
		{
			min_energy = energy;
			cpufreq_best = cpufreq_chosen;
			gpufreq_best = gpufreq_chosen;
		}
	}
	

	// Return the best values in the two pointers
	*cyclesolo_gpufreq = gpufreq_best;
	*cyclesolo_cpufreq = cpufreq_best;
	return 0;
}