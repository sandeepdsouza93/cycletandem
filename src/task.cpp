/*
 * @file task.cpp
 * @brief Task class implementation
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <iostream>
#include <cmath>

// Include Internal Headers
#include "task.hpp"

// Constructor
Task::Task(task_t task_params)
{
	params = task_params;
	cpu_freq = 1.0;
	gpu_freq = 1.0;
	core_id = 0;
}

// Destructor
Task::~Task()
{
	return;
}

// Get the task parameters
double Task::getC() const 
{
	return params.C;
}

double Task::getGm() const 
{
	return params.Gm;
}

double Task::getGe() const
{
	return params.Ge;
}

double Task::getD() const 
{
	return params.D;
}

double Task::getT() const 
{
	return params.T;
}

double Task::getCpuFreq() const
{
	return cpu_freq;
}

double Task::getGpuFreq() const
{
	return gpu_freq;
} 

unsigned int Task::getCoreID() const
{
	return core_id;
}

int Task::setCoreID(unsigned int coreID)
{
	core_id = coreID;
}

int Task::scale_cpu(double cpu_frequency)
{
	if (cpu_frequency > 1)
		return -1;

	// Check if current cpu frequency is 1
	if (cpu_freq == 1)
	{
		params.C = params.C/cpu_frequency;
		params.Gm = params.Gm/cpu_frequency;
		cpu_freq = cpu_frequency;
	}
	else
	{
		// First scale to bring to frequency = 1, the apply new scaling factor
		params.C = params.C*cpu_freq/cpu_frequency;
		params.Gm = params.Gm*cpu_freq/cpu_frequency;
		cpu_freq = cpu_frequency;
	}
	return 0;
}

int Task::scale_gpu(double gpu_frequency)
{
	if (gpu_frequency > 1)
		return -1;

	// Check if current cpu frequency is 1
	if (gpu_freq == 1)
	{
		params.Ge = params.Ge/gpu_frequency;
		gpu_freq = gpu_frequency;
	}
	else
	{
		// First scale to bring to frequency = 1, the apply new scaling factor
		params.Ge = params.Ge*gpu_freq/gpu_frequency;
		gpu_freq = gpu_frequency;
	}
	return 0;
}

int Task::task_timescale(int scaling_factor)
{
	params.C = std::floor(params.C*scaling_factor);
	params.Gm = std::floor(params.Gm*scaling_factor);
	params.Ge = std::floor(params.Ge*scaling_factor);
	params.D = std::floor(params.D*scaling_factor);
	params.T = std::floor(params.T*scaling_factor);
	return 0;
}



