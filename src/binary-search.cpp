/*
 * @file binary-search.cpp
 * @brief Binary-search-based frequency calculation algorithms
 * @author Sandeep D'souza 
 * 
 * Copyright (c) Carnegie Mellon University, 2018. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 	1. Redistributions of source code must retain the above copyright notice, 
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, 
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "binary-search.hpp"
#include "taskset.hpp"
#include "request-driven-test.hpp"

/**************** Binary Search to return minimum CPU frequency ********************/ 
/* Params: task_vector: vector of tasks 
           epsilon    : convergence criterion
   Returns: minimum CPU frequency */
double binary_search_cpu_frequency(const std::vector<Task> &task_vector, double epsilon, double gpu_freq)
{
	double f_up = 1;
	double f_low = 0;
	double f_est = 0.5;
	int sched_flag = 0;

	while (f_up - f_low >= epsilon)
	{
		std::vector<Task> scaled_task_vector = scale_taskset_frequency(task_vector, f_est, gpu_freq);
		sched_flag = check_schedulability_request_driven(scaled_task_vector);
		if (sched_flag == 0)
		{
			f_up = f_est;
		}
		else
		{
			f_low = f_est;
		}
		f_est = (f_up + f_low)/2;
	}
	return f_up;
}

/**************** Binary Search to return minimum GPU frequency ********************/ 
/* Params: task_vector: vector of tasks 
           epsilon    : convergence criterion
   Returns: minimum GPU frequency */
double binary_search_gpu_frequency(const std::vector<Task> &task_vector, double epsilon, double cpu_freq)
{
	double f_up = 1;
	double f_low = 0;
	double f_est = 0.5;
	int sched_flag = 0;

	while (f_up - f_low >= epsilon)
	{
		std::vector<Task> scaled_task_vector = scale_taskset_frequency(task_vector, cpu_freq, f_est);
		sched_flag = check_schedulability_request_driven(scaled_task_vector);
		if (sched_flag == 0)
		{
			f_up = f_est;
		}
		else
		{
			f_low = f_est;
		}
		f_est = (f_up + f_low)/2;
	}
	return f_up;
}

/**************** Binary Search to return minimum common CPU-GPU frequency ********************/ 
/* Params: task_vector: vector of tasks 
           epsilon    : convergence criterion
   Returns: minimum common CPU-GPU frequency */
double binary_search_common_frequency(const std::vector<Task> &task_vector, double epsilon)
{
	double f_up = 1;
	double f_low = 0;
	double f_est = 0.5;
	int sched_flag = 0;

	while (f_up - f_low >= epsilon)
	{
		std::vector<Task> scaled_task_vector = scale_taskset_frequency(task_vector, f_est, f_est);
		sched_flag = check_schedulability_request_driven(scaled_task_vector);
		if (sched_flag == 0)
		{
			f_up = f_est;
		}
		else
		{
			f_low = f_est;
		}
		f_est = (f_up + f_low)/2;
	}
	return f_up;
}

/**************** Binary Search to return minimum CPU frequency in a range ********************/ 
/* Params: task_vector: vector of tasks 
           epsilon    : convergence criterion
           f_up       : upper bound
           f_low      : lower bound
   Returns: minimum CPU frequency */
double binary_search_cpu_frequency_range(const std::vector<Task> &task_vector, double epsilon, double gpu_freq, double f_up, double f_low)
{
	double f_est = 0.5;
	int sched_flag = 0;

	while (f_up - f_low >= epsilon)
	{
		std::vector<Task> scaled_task_vector = scale_taskset_frequency(task_vector, f_est, gpu_freq);
		sched_flag = check_schedulability_request_driven(scaled_task_vector);
		if (sched_flag == 0)
		{
			f_up = f_est;
		}
		else
		{
			f_low = f_est;
		}
		f_est = (f_up + f_low)/2;
	}
	return f_up;
}

/**************** Binary Search to return minimum GPU frequency in a range ********************/ 
/* Params: task_vector: vector of tasks 
           epsilon    : convergence criterion
           f_up       : upper bound
           f_low      : lower bound
   Returns: minimum GPU frequency */
double binary_search_gpu_frequency_range(const std::vector<Task> &task_vector, double epsilon, double cpu_freq, double f_up, double f_low)
{
	double f_est = 0.5;
	int sched_flag = 0;

	while (f_up - f_low >= epsilon)
	{
		std::vector<Task> scaled_task_vector = scale_taskset_frequency(task_vector, cpu_freq, f_est);
		sched_flag = check_schedulability_request_driven(scaled_task_vector);
		if (sched_flag == 0)
		{
			f_up = f_est;
		}
		else
		{
			f_low = f_est;
		}
		f_est = (f_up + f_low)/2;
	}
	return f_up;
}

/**************** Binary Search to return minimum common CPU-GPU frequency in a range ********************/ 
/* Params: task_vector: vector of tasks 
           epsilon    : convergence criterion
           f_up       : upper bound
           f_low      : lower bound
   Returns: minimum common CPU-GPU frequency */
double binary_search_common_frequency_range(const std::vector<Task> &task_vector, double epsilon, double f_up, double f_low)
{
	double f_est = 0.5;
	int sched_flag = 0;

	while (f_up - f_low >= epsilon)
	{
		std::vector<Task> scaled_task_vector = scale_taskset_frequency(task_vector, f_est, f_est);
		sched_flag = check_schedulability_request_driven(scaled_task_vector);
		if (sched_flag == 0)
		{
			f_up = f_est;
		}
		else
		{
			f_low = f_est;
		}
		f_est = (f_up + f_low)/2;
	}
	return f_up;
}

/**************** Binary Search to return minimum CPU frequency in a range for a particular core ********************/ 
/* Params: task_vector: vector of tasks 
           epsilon    : convergence criterion
           gpu_freq   : gpu frequency
           f_up       : upper bound
           f_low      : lower bound
           core_freq  : vector of per-core CPU frequencies
           core_id    : core id to perform search on
   Returns: minimum CPU frequency for the core id and updates the new frequency in the core_freq vector */
double binary_search_per_core_cpu_frequency_range(const std::vector<Task> &task_vector, double epsilon, double gpu_freq, double f_up, double f_low,
												  std::vector<double> core_freq, int core_id)
{
	double f_est = (f_up + f_low)/2;
	int sched_flag = 0;

	while (f_up - f_low >= epsilon)
	{
		// Set the frequency of the core we are searching on
		core_freq[core_id] = f_est;

		// Scale the taskset frequency and check the schedulability
		std::vector<Task> scaled_task_vector = scale_taskset_per_core_frequency(task_vector, core_freq, gpu_freq);
		sched_flag = check_schedulability_request_driven(scaled_task_vector);
		if (sched_flag == 0)
		{
			f_up = f_est;			
		}
		else
		{
			f_low = f_est;
		}
		f_est = (f_up + f_low)/2;
	}
	// Set the frequency of the core we are searching on
	core_freq[core_id] = f_up;
	return f_up;
}

